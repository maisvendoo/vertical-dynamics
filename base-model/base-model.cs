﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace base_model
{
    public abstract class CBaseModel
    {
        protected const double g = 9.81;

        protected int n = 1;
        protected double[] dYdt = null;

        // Springs parameters
        protected double fs1 = 0.12;
        protected double fs2 = 0.12;
        protected double gamma1 = 0.1;
        protected double gamma2 = 0.1;

        // Inertia parameters
        protected double m1 = 8000.0; // Trolley mass
        protected double m2 = 47000.0; // Body mass
        protected double I1 = 1e7; // Trolley moment of inertia
        protected double I2 = 1e3; // Body moment of inertia

        // Mass geometry
        protected double a = 1.2;
        protected double Lb = 9.0;

        // Velocity
        protected double v = 55.6;

        // Railway params
        protected double L = 20.0;
        protected double eta_m = 5e-3;
        protected bool random_law = false;

        public CBaseModel()
        {

        }

        public int get_dimention()
        {
            return n;
        }

        abstract public void init();
        public abstract void dFdt(double[] Y, ref double[] dYdt, double t);
        public virtual void pre_step(ref double[] Y, double t) { }
        public virtual void post_step(ref double[] Y, double t) { }

        public void set_body_mass(double m2)
        {
            this.m2 = m2;
        }

        public void set_trolley_mass(double m1)
        {
            this.m1 = m1;
        }

        public void set_trolley_moment_of_inertia(double I1)
        {
            this.I1 = I1;
        }

        public void set_body_moment_of_inertia(double I2)
        {
            this.I2 = I2;
        }

        public void set_stage1_static_deformation(double fs1)
        {
            this.fs1 = fs1;
        }

        public void set_stage2_static_deformation(double fs2)
        {
            this.fs2 = fs2;
        }

        public void set_stage1_damp_coeff(double gamma1)
        {
            this.gamma1 = gamma1;
        }

        public void set_stage2_damp_coeff(double gamma2)
        {
            this.gamma2 = gamma2;
        }

        public void set_trolley_half_base(double a)
        {
            this.a = a;
        }

        public void set_body_half_base(double Lb)
        {
            this.Lb = Lb;
        }

        public void set_velocity(double v)
        {
            this.v = v;
        }

        public void set_wave_length(double L)
        {
            this.L = L;
        }

        public void set_amplify(double eta_m)
        {
            this.eta_m = eta_m;
        }

        public void set_random_law(bool random_law)
        {
            this.random_law = random_law;
        }
    }
}
