﻿using System;

namespace core
{
    public delegate bool SolverStep(ref double[] Y, ref double[] dYdt, double t, ref double dt, double dt_max, double err, OdeSystem sys);

    public abstract class Solver
    {
        protected const int MAX_ITER = 100;
        public Solver()
        {

        }
        public abstract bool step(ref double[] Y, ref double[] dYdt, double t, ref double dt, double dt_max, double err, OdeSystem ode_sys);
    }
}