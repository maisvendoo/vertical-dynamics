﻿using System;

namespace core
{
    public class EulerSolver : Solver
    {
        int n = 1;
        bool first_step = true;

        //---------------------------------------------------------------------
        //      Euler integration step
        //---------------------------------------------------------------------
        public override bool step(ref double[] Y,
                                  ref double[] dYdt,
                                  double t,
                                  ref double dt,
                                  double dt_max,
                                  double err,
                                  OdeSystem ode_sys)
        {
            if (first_step)
            {
                n = Y.Length;
                first_step = false;
            }

            ode_sys(Y, ref dYdt, t);

            for (int i = 0; i < n; i++)
                Y[i] += dYdt[i] * dt;

            return true;
        }
    }
}