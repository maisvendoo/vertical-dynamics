﻿using System;

namespace core
{
    public class RK4Solver: Solver
    {
        double[] k1;
        double[] k2;
        double[] k3;
        double[] k4;
        double[] Y1;

        int n;

        bool first_step;

        public RK4Solver()
        {
            first_step = true;
        }

        public override bool step(ref double[] Y, ref double[] dYdt, double t, ref double dt, double dt_max, double err, OdeSystem ode_sys)
        {
            if (first_step)
            {
                n = Y.Length;
                k1 = new double[n];
                k2 = new double[n];
                k3 = new double[n];
                k4 = new double[n];
                Y1 = new double[n];

                first_step = false;
            }

            ode_sys(Y, ref dYdt, t);

            for (int i = 0; i < n; i++)
            {

                k1[i] = dYdt[i];
                Y1[i] = Y[i] + k1[i] * dt / 2;
            }

            ode_sys(Y1, ref dYdt, t + dt / 2);

            for (int i = 0; i < n; i++)
            {

                k2[i] = dYdt[i];
                Y1[i] = Y[i] + k2[i] * dt / 2;
            }

            ode_sys(Y1, ref dYdt, t + dt / 2);

            for (int i = 0; i < n; i++)
            {

                k3[i] = dYdt[i];
                Y1[i] = Y[i] + k3[i] * dt;
            }

            ode_sys(Y1, ref dYdt, t + dt);

            for (int i = 0; i < n; i++)
            {

                k4[i] = dYdt[i];
                Y[i] += dt * (k1[i] + 2 * k2[i] + 2 * k3[i] + k4[i]) / 6;
            }

            return true;
        }
    }
}