﻿using System;

namespace core
{
    public class RKF5Solver : Solver
    {
        const double c1 = 16.0 / 135;
        const double c3 = 6656.0 / 12825;
        const double c4 = 28561.0 / 56430;
        const double c5 = -9.0 / 50;
        const double c6 = 2.0 / 55;

        const double b21 = 0.25;

        const double b31 = 3.0 / 32;
        const double b32 = 9.0 / 32;

        const double b41 = 1932.0 / 2197;
        const double b42 = -7200.0 / 2197;
        const double b43 = 7296.0 / 2197;

        const double b51 = 439.0 / 216;
        const double b52 = -8.0;
        const double b53 = 3680.0 / 513;
        const double b54 = -845.0 / 4140;

        const double b61 = -8.0 / 27;
        const double b62 = 2.0;
        const double b63 = -3544.0 / 2565;
        const double b64 = 1859.0 / 4104;
        const double b65 = -11.0 / 40;

        const double e1 = 1.0 / 360;
        const double e3 = -128.0 / 4275;
        const double e4 = -2197.0 / 75240;
        const double e5 = 1.0 / 50;
        const double e6 = 2.0 / 55;

        double[] k1;
        double[] k2;
        double[] k3;
        double[] k4;
        double[] k5;
        double[] k6;
        double[] Y1;        
        double[] dY1dt;
        double[] eps_y;

        int n;
        bool first_step;

        public RKF5Solver()
        {
            first_step = true;
        }

        public override bool step(ref double[] Y, ref double[] dYdt, double t, ref double dt, double dt_max, double err, OdeSystem ode_sys)
        {
            if (first_step)
            {
                n = Y.Length;
                k1 = new double[n];
                k2 = new double[n];
                k3 = new double[n];
                k4 = new double[n];
                k5 = new double[n];
                k6 = new double[n];
                Y1 = new double[n];
                dY1dt = new double[n];
                eps_y = new double[n];

                first_step = false;
            }

            bool ready = false;
            int iter = 0;
            double delta = 0;

            do
            {
                delta = 0;
                iter++;

                ode_sys(Y, ref dYdt, t);

                for (int i = 0; i < n; i++)
                {
                    //dY1dt[i] = dYdt[i];
                    k1[i] = dt * dYdt[i];
                    Y1[i] = Y[i] + b21 * k1[i];
                }

                ode_sys(Y1, ref dYdt, t + dt / 4.0);

                for (int i = 0; i < n; i++)
                {
                    k2[i] = dt * dYdt[i];
                    Y1[i] = Y[i] + b31 * k1[i] + b32 * k2[i];
                }

                ode_sys(Y1, ref dYdt, t + 3.0 * dt / 8.0);

                for (int i = 0; i < n; i++)
                {
                    k3[i] = dt * dYdt[i];
                    Y1[i] = Y[i] + b41 * k1[i] + b42 * k2[i] + b43 * k3[i];
                }

                ode_sys(Y1, ref dYdt, t + 12.0 * dt / 13.0);

                for (int i = 0; i < n; i++)
                {
                    k4[i] = dt * dYdt[i];
                    Y1[i] = Y[i] + b51 * k1[i] + b52 * k2[i] + b53 * k3[i] +
                        b54 * k4[i];
                }

                ode_sys(Y1, ref dYdt, t + dt);

                for (int i = 0; i < n; i++)
                {
                    k5[i] = dt * dYdt[i];
                    Y1[i] = Y[i] + b61 * k1[i] + b62 * k2[i] + b63 * k3[i] +
                        b64 * k4[i] + b65 * k5[i];
                }

                ode_sys(Y1, ref dYdt, t + dt / 2.0);

                for (int i = 0; i < n; i++)
                {
                    k6[i] = dt * dYdt[i];
                    eps_y[i] = Math.Abs(e1 * k1[i] + e3 * k3[i] + e4 * k4[i] +
                        e5 * k5[i] + e6 * k6[i]);

                    if (delta < eps_y[i])
                        delta = eps_y[i];
                }

                if (delta >= err)
                {
                    dt = dt / 2;
                    ready = false;
                }

                if (delta <= err / 32.0)
                {
                    dt = 2 * dt;

                    if (dt > dt_max)
                        dt = dt_max;

                    ready = true;
                }

                if ((delta > err / 32.0) && (delta < err))
                {
                    ready = true;
                }


                if (ready)
                {
                    for (int i = 0; i < n; i++)
                        Y[i] = Y[i] + c1 * k1[i] + c3 * k3[i] + c4 * k4[i] +
                            c5 * k5[i] + c6 * k6[i];
                }

            } while ((!ready) && (iter <= MAX_ITER));

            if (iter > MAX_ITER)
                return false;

            return true;
        }
    }
}