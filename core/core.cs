﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace core
{
    public delegate void OdeSystem(double[] Y, ref double[] dYdt, double t);
    public delegate void PreStep(ref double[] Y, double t);
    public delegate void PostStep(ref double[] Y, double t);

    public class Simulator
    {
        protected double[] y;
        protected double[] dydt;
        protected double[] initc_y;
        protected int n;

        protected double t;
        protected double dt;
        protected double max_dt;
        protected double t_begin;
        protected double t_end;

        protected double local_err;

        protected OdeSystem ode_sys;
        protected SolverStep solver_step;
        protected PreStep pre_step;
        protected PostStep post_step;

        protected Thread sim_thread;  
        
        protected bool is_correct = true;

        protected string sim_status;

        //---------------------------------------------------------------------
        //      Simulator initialization
        //---------------------------------------------------------------------
        public void init(int n)
        {
            this.n = n;
            
            // Create state vector
            y = new double[n];
            dydt = new double[n];
            initc_y = new double[n];

            // Zero initial conditions
            for (int i = 0; i < n; i++)
            {
                y[i] = initc_y[i] = 0;
                dydt[i] = 0;
            }
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void reset()
        {
            t = t_begin;
            dt = max_dt;

            for (int i = 0; i < n; i++)
            {
                y[i] = initc_y[i];
                dydt[i] = 0;
            }
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void set_ode_system(OdeSystem ode_sys)
        {
            this.ode_sys = ode_sys;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void set_method(SolverStep solver_step)
        {
            this.solver_step = solver_step;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void set_pre_step(PreStep pre_step)
        {
            this.pre_step = pre_step;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void set_post_step(PostStep post_step)
        {
            this.post_step = post_step;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void set_init_time(double t_begin)
        {
            this.t_begin = t_begin;
            this.t = t_begin;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void set_end_time(double t_end)
        {
            this.t_end = t_end;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void set_time_step(double max_dt)
        {
            this.max_dt = max_dt;
            this.dt = max_dt;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void set_local_error(double local_err)
        {
            this.local_err = local_err;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void set_y(int idx, double value)
        {
            initc_y[idx] = value;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public double get_y(int idx)
        {
            return y[idx];
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public string get_status()
        {
            return sim_status;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void loop()
        {
            reset();

            while ( (t <= t_end) && (is_correct) )
            {
                is_correct = process();                
            }

            if (is_correct)
                sim_status = "Simulation complete";
            else
                sim_status = "FAIL: Method iterations limit";
        }

        public virtual bool process()
        {
            pre_step(ref y, t);
            bool step_quality = step();
            post_step(ref y, t);

            t += dt;

            return step_quality;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void start()
        {
            sim_thread = new Thread(this.loop);
            sim_thread.Start();
        }

        //----------------------------------------------------------------------
        //
        //----------------------------------------------------------------------
        public void join()
        {
            sim_thread.Join();
        }

        //----------------------------------------------------------------------
        //
        //----------------------------------------------------------------------
        protected bool step()
        {
            return solver_step(ref y, ref dydt, t, ref dt, max_dt, local_err, ode_sys);
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public double get_time()
        {
            return t;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public double get_end_time()
        {
            return t_end;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public double get_step()
        {
            return dt;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public double[] get_dydt(double[] Y, double t)
        {
            double[] dYdt = new double[n];

            ode_sys(Y, ref dYdt, t);

            return dYdt;
        }

        public double[] get_y()
        {
            return y;
        }
    }
}
