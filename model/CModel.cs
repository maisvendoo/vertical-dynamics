﻿using System;
using base_model;

namespace model
{
    public class CModel : CBaseModel
    {
        private double c1;
        private double c2;
        private double beta1;
        private double beta2;

        private double phi1;
        private double phi2;
        private double phi3;
        private double phi4;
        private double omega;
        

        public CModel()
        {
            n = 12;
        }

        public override void init()
        {
            // Shiffnesses calculation
            c1 = (2 * m1 + m2) * g / 4 / fs1;             
            c2 = m2 * g / 2 / fs2;            

            // Critical damp calculation
            double b_cr1 = 2 * Math.Sqrt(2 * c1 * m1);
            double b_cr2 = 2 * Math.Sqrt(2 * c2 * m2);

            // Damp calculation
            beta1 = gamma1*b_cr1;
            beta2 = gamma2*b_cr2;
                        
            omega = 2 * Math.PI * v / L;

            phi1 = 0;
            phi2 = omega * 2 * a / v;
            phi3 = omega * 2 * Lb / v;
            phi4 = omega * (2 * Lb + a) / v;
        }

        public override void dFdt(double[] Y, ref double[] dYdt, double t)
        {
            dYdt[0] = Y[6];
            dYdt[1] = Y[7];
            dYdt[2] = Y[8];
            dYdt[3] = Y[9];
            dYdt[4] = Y[10];
            dYdt[5] = Y[11];

            double T0 = -(2 * c1 + c2) * Y[0] + c2 * Y[2] + c2 * Lb * Y[5];
            double T1 = -(2 * c1 + c2) * Y[1] + c2 * Y[2] - c2 * Lb * Y[5];
            double T2 = c2 * Y[0] + c2 * Y[1] - 2 * c2 * Y[2];
            double T3 = -2 * a * a * c1 * Y[3];
            double T4 = -2 * a * a * c1 * Y[4];
            double T5 = a * c2 * Y[0] - a * c2 * Y[1] - 2 * a * c2 * Lb * Y[5];
            
            double R0 = -(2 * beta1 + beta2) * Y[6] + beta2 * Y[8] + beta2 * Lb * Y[11];
            double R1 = -(2 * beta1 + beta2) * Y[7] + beta2 * Y[8] - beta2 * Lb * Y[11];
            double R2 = beta2 * Y[6] + beta2 * Y[7] - 2 * beta2 * Y[8];
            double R3 = -2 * a * a * beta1 * Y[9];
            double R4 = -2 * a * a * beta1 * Y[10];
            double R5 = a * beta2 * Y[6] - a * beta2 * Y[7] - 2 * a * beta2 * Lb * Y[11];

            double h1 = eta_m *(1 -  Math.Cos(omega * t - phi1));
            double h2 = eta_m * (1 - Math.Cos(omega * t - phi2));
            double h3 = eta_m * (1 - Math.Cos(omega * t - phi3));
            double h4 = eta_m * (1 - Math.Cos(omega * t - phi4)); 

            double dh1dt = omega * eta_m * Math.Sin(omega * t + phi1);
            double dh2dt = omega * eta_m * Math.Sin(omega * t + phi2);
            double dh3dt = omega * eta_m * Math.Sin(omega * t + phi3);
            double dh4dt = omega * eta_m * Math.Sin(omega * t + phi4);

            double Q1 = beta1 * dh1dt + beta1 * dh2dt + c1*h1 + c1*h2;
            double Q2 = beta1 * dh3dt + beta1 * dh4dt + c1 * h3 + c1 * h4;
            double Q3 = 0;
            double Q4 = a * beta1 * dh1dt - a * beta1 * dh2dt + a * c1 * h1 - a * c1 * h2;
            double Q5 = a * beta1 * dh3dt - a * beta1 * dh4dt + a * c1 * h3 - a * c1 * h4;
            double Q6 = 0;

            dYdt[6] = (T0 + R0 + Q1) / m1;
            dYdt[7] = (T1 + R1 + Q2) / m1;
            dYdt[8] = (T2 + R2 + Q3) / m2;
            dYdt[9] = (T3 + R3 + Q4) / I1;
            dYdt[10] = (T4 + R4 + Q5) / I1;
            dYdt[11] = (T5 + R5 + Q6) / I2;
        }        
    }
}
