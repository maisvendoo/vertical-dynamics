;------------------------------------------------------------------------------
;
;     Installer for Vertical Dynamics
;     (c) maisvendoo, 10.03.2016   
;
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;     Some globals
;------------------------------------------------------------------------------
#define   Name      "Vertical Dynamics"
#define   Version   "0.1.0"
#define   Publisher "RSTU"
#define   URL       "http://maisvendoo.org"
#define   ExeName   "solver.exe"

;------------------------------------------------------------------------------
;     Setup parameters
;------------------------------------------------------------------------------
[Setup]
AppId = {{B2EDCAC0-A723-4144-AC7E-F6A2C5B2FF10}

AppName={#Name}
AppVersion={#Version}
AppPublisher={#Publisher}
AppPublisherURL={#URL}
AppSupportURL={#URL}
AppUpdatesURL={#URL}

DefaultDirName={pf}\{#Name}
DefaultGroupName={#Name}

OutputDir=E:\work\vertical-dynamics\setup-bin
OutputBaseFileName=vdsetup

SetupIconFile=E:\work\vertical-dynamics\src\vertical-dynamics\solver\logo.ico

Compression=lzma
SolidCompression=yes
PrivilegesRequired=admin

;------------------------------------------------------------------------------
;     Languages support
;------------------------------------------------------------------------------
[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"; LicenseFile: "LICENSE_EN.txt"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"; LicenseFile: "LICENSE_RU.txt"

;------------------------------------------------------------------------------
;     Optional tasks
;------------------------------------------------------------------------------
[Tasks]
; Desktop icon creation
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked


;------------------------------------------------------------------------------
;     Distribution files
;------------------------------------------------------------------------------ 
[Files]

; Executable
Source: "E:\work\vertical-dynamics\bin\solver.exe"; DestDir: "{app}\bin\"; Flags: ignoreversion

; Other binary code and resources
Source: "E:\work\vertical-dynamics\bin\*"; DestDir: "{app}\bin"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "E:\work\vertical-dynamics\cfg\*"; DestDir: "{app}\cfg"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "E:\work\vertical-dynamics\gnuplot\*"; DestDir: "{app}\gnuplot"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "E:\work\vertical-dynamics\results\*"; DestDir: "{app}\results"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "E:\work\vertical-dynamics\LICENSE.rtf"; DestDir: "{app}\"; Flags: ignoreversion recursesubdirs createallsubdirs

 [Icons]

 Name: "{group}\{#Name}"; Filename: "{app}\bin\{#ExeName}"
 Name: "{commondesktop}\{#Name}"; Filename: "{app}\bin\{#ExeName}"; Tasks: desktopicon