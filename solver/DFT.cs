﻿using System;
using System.Collections.Generic;

namespace solver
{
    public class DFT
    {
        public DFT()
        {

        }

        public static Complex[] get_spectrum(List<SpringData> spring_data_list, double T, ref double main_omega)
        {
            int N = spring_data_list.Count / 2;
            Complex[] X = new Complex[N];
            double arg = 0;
            double A_max = 0;

            for (int k = 0; k < N; k++)
            {
                X[k] = new Complex();
                X[k].Re = X[k].Im = 0;

                for (int j = 0; j < N; j++)
                {
                    arg = 2 * Math.PI * k * j / N;
                    X[k].Re += spring_data_list[j+N].S[4] * Math.Cos(arg);
                    X[k].Im -= spring_data_list[j+N].S[4] * Math.Sin(arg);                    
                }

                X[k].Amplify = X[k].Abs / N;
                X[k].omega = 2 * Math.PI * k / T;


                if ( (X[k].Amplify > A_max) && (k != 0) )
                {
                    A_max = X[k].Amplify;
                    main_omega = X[k].omega;
                }
                    
            }

            return X;   
        }
    }

}