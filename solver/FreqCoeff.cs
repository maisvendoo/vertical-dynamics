﻿using System;

namespace solver
{
    public class FreqCoeff
    {
        public FreqCoeff()
        {

        }

        private static double[,] Kf =
        {
            {0.0, 0.0 },
            { 1.0503294450791767, 0.9532620869289581 },
            { 1.387854244079385, 0.9775237184151218 },
            { 1.6865680122549649, 1.0048450939825964 },
            { 1.9776754842455189, 1.0296039191340218 },
            { 2.2929574407363607, 1.0543952934612177 },
            { 2.5894192235390934, 1.0790441149834569 },
            { 2.909690528203421, 1.1013212942990738 },
            { 3.24262858706364, 1.1248307003433928 },
            { 3.6100222792372776, 1.1477465583880582 },
            { 3.9753673921633563, 1.1738225096767743 },
            { 4.38740518421464, 1.1943007273191648 },
            { 4.773408281321848, 1.2187639773749384 },
            { 5.22656311718076, 1.238117064548741 },
            { 5.6707410578143564, 1.2451633448581003 },
            { 6.113885162988103, 1.2453992340756999 },
            { 6.55641770352363, 1.2416064412390189 },
            { 6.998163946667157, 1.2326339143325484 },
            { 7.439910189810684, 1.2236613874260782 },
            { 7.8814962612632495, 1.2136337295053916 },
            { 8.322907599962036, 1.2024550195691963 },
            { 8.764304377598009, 1.1911803886317087 },
            { 9.2058176437365, 1.1806731257045602 },
            { 9.647199860309657, 1.1693025737657798 },
            { 10.088640321134074, 1.1583157058321694 },
            { 10.530037098770046, 1.1470410748946818 },
            { 10.971477559594463, 1.1360542069610713 },
            { 11.413049069984213, 1.1259306280390922 },
            { 11.854649702499593, 1.115998891119698 },
            { 12.296221212889343, 1.105875312197719 },
            { 12.737792723279094, 1.09575173327574 },
            { 13.179378794731658, 1.0857240753550534 },
            { 13.62122696531489, 1.07742299545763 },
            { 14.06307513589812, 1.0691219155602065 },
            { 14.504908745418536, 1.0607249146614905 },
            { 14.946756916001767, 1.052423834764067 },
            { 15.388794380401588, 1.0453697278834446 },
            { 15.830977455429561, 1.0392748310157462 },
            { 16.27310228620627, 1.0327962501428782 },
            { 16.7152416780458, 1.0264135902713025 },
            { 17.157366508822513, 1.0199350093984345 },
            { 17.59989904935804,1.0161422165617533 },
            { 18.042664566898598, 1.0138841597457506 },
            { 18.485284473811014, 1.010666892916824 },
            { 18.927991747100318, 1.0080251520956518 },
            { 19.370597092949918, 1.0047119642654327 },
            { 19.793100163530564, 1.001655760992566 },
            { 1000.0, 1.0 } 
        };

        public static double get_Kf(double omega)
        {
            int i = 0;
            int n = Kf.Length;
            double f = omega / 2.0 / Math.PI;
            bool found = false;
            int k = 0;

            if (f < 0)
                return 0.0;

            do
            {
                if ((f >= Kf[i, 0]) && (f < Kf[i + 1, 0]))
                {
                    found = true;
                    k = i;
                }

                i++;

            } while ((i < n - 1) && (!found));

            if (i >= n-1)
                return 1.0;

            return Kf[k, 1] + (Kf[k + 1, 1] - Kf[k, 1]) * (f - Kf[k, 0]) / (Kf[k + 1, 0] - Kf[k, 0]);
        }
    }
}