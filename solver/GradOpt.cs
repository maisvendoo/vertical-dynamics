﻿using System;

namespace solver
{
    public delegate double Criterium(double[] x);
    public class GradOpt : Optimum
    {
        public GradOpt()
        {

        }         

        public override double step(ref double[] x, double h, double lambda, ref double J1)
        {
            double delta = 0;

            int n = x.Length;
            double[] x1 = new double[n];
            double[] dJdx = new double[n];
             
            double J0 = J1 = J(x);
            double sum = 0;

            // Grad calculation
           for (int i = 0; i < n; i++)
           {
                for (int j = 0; j < n; j++)
                {
                    if (i == j)
                        x1[j] = x[j] + h;
                    else
                        x1[j] = x[j];
                }

                dJdx[i] = (J(x1) - J0) / h;

                sum += dJdx[i] * dJdx[i];
           }

            for (int i = 0; i < n; i++)
            {
                x[i] -= lambda * dJdx[i] / Math.Sqrt(sum);
                x[i] = limitation(x[i], 0.0, 1.0);
            }

            delta = Math.Sqrt(sum);

            return delta;           
        }

        public override bool read_config(string cfg_pafh)
        {
            return true;
        }
    }
}