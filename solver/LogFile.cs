﻿//-----------------------------------------------------------------------------
//
//
//
//-----------------------------------------------------------------------------
using System;
using System.IO;

namespace solver
{
    public delegate void LogPrintFunc(StreamWriter writer);

    //-------------------------------------------------------------------------
    //
    //-------------------------------------------------------------------------
    public class LogFile
    {
        private StreamWriter writer;
        public LogPrintFunc print_func;

        private double print_time = 0;
        private bool first_print = true;        

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public LogFile ()
        {
            writer = new StreamWriter(Console.OpenStandardOutput ());
            writer.AutoFlush = true;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public LogFile(string FileName)
        {
            writer = new StreamWriter (FileName);
            writer.AutoFlush = true;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void print(double dt, double DeltaT)
        {
            if ((print_time >= DeltaT) || (first_print)) {

                print_time = 0;
                first_print = false;
                print_func (writer);
            }

            print_time += dt;
        }        
        

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void close()
        {
            writer.Close();
        }       
    }
}

