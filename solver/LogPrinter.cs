﻿namespace solver
{
    public delegate void PrintFunc();
    public class LogPrinter
    {
        bool first_print = true;
        double print_time = 0;

        public PrintFunc print_func;

        public LogPrinter()
        {

        }

        public void reset()
        {
            first_print = false;
            print_time = 0;
        }

        public void process(double dt, double DeltaT)
        {
            if ( (first_print) || (print_time >= DeltaT) )
            {
                first_print = false;
                print_time = 0;

                print_func();
            }

            print_time += dt;
        }        
    }
}