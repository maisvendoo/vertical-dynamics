﻿namespace solver
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.itemFile = new System.Windows.Forms.ToolStripMenuItem();
            this.subitemQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.itemSim = new System.Windows.Forms.ToolStripMenuItem();
            this.subitemStart = new System.Windows.Forms.ToolStripMenuItem();
            this.subitemPause = new System.Windows.Forms.ToolStripMenuItem();
            this.subitemStop = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.subitemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.subitemOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.itemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.MainStatus = new System.Windows.Forms.StatusStrip();
            this.solverStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.optProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.SolverPanel = new System.Windows.Forms.Panel();
            this.gDynQuality = new System.Windows.Forms.GroupBox();
            this.changeSaveFile = new System.Windows.Forms.Button();
            this.dyncalcStop = new System.Windows.Forms.Button();
            this.dyncalcPause = new System.Windows.Forms.Button();
            this.dyncalcStart = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.VelocityStep = new System.Windows.Forms.TextBox();
            this.MaxVelocity = new System.Windows.Forms.TextBox();
            this.InitVelocity = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.optSendParams = new System.Windows.Forms.Button();
            this.optStop = new System.Windows.Forms.Button();
            this.optPause = new System.Windows.Forms.Button();
            this.Accuracy = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Gamma2Max = new System.Windows.Forms.TextBox();
            this.Gamma2Min = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Gamma1Max = new System.Windows.Forms.TextBox();
            this.Gamma1Min = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Fs2Max = new System.Windows.Forms.TextBox();
            this.Fs2Min = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Fs1Max = new System.Windows.Forms.TextBox();
            this.Fs1Min = new System.Windows.Forms.TextBox();
            this.optStart = new System.Windows.Forms.Button();
            this.gRailwayParams = new System.Windows.Forms.GroupBox();
            this.RandomLaw = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Amplify = new System.Windows.Forms.TextBox();
            this.WaveLength = new System.Windows.Forms.TextBox();
            this.MainToolBar = new System.Windows.Forms.ToolStrip();
            this.gVehicleParams = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LSpringsParams = new System.Windows.Forms.Label();
            this.Stage2DampCoeff = new System.Windows.Forms.TextBox();
            this.Stage1DampCoeff = new System.Windows.Forms.TextBox();
            this.Stage2StaticDeformation = new System.Windows.Forms.TextBox();
            this.Stage1StaticDeformation = new System.Windows.Forms.TextBox();
            this.LVelocity = new System.Windows.Forms.Label();
            this.LTrolleyHeight = new System.Windows.Forms.Label();
            this.LTrolleyBase = new System.Windows.Forms.Label();
            this.LBodyHeight = new System.Windows.Forms.Label();
            this.LBodyBase = new System.Windows.Forms.Label();
            this.LTrolleyCoeff = new System.Windows.Forms.Label();
            this.LBodyMass = new System.Windows.Forms.Label();
            this.ConstructVelocity = new System.Windows.Forms.TextBox();
            this.TrolleyHeight = new System.Windows.Forms.TextBox();
            this.BodyHeight = new System.Windows.Forms.TextBox();
            this.TrolleyBase = new System.Windows.Forms.TextBox();
            this.BodyBase = new System.Windows.Forms.TextBox();
            this.TrolleyCoeff = new System.Windows.Forms.TextBox();
            this.VehicleMass = new System.Windows.Forms.TextBox();
            this.LogBox = new System.Windows.Forms.RichTextBox();
            this.savePDKlog = new System.Windows.Forms.SaveFileDialog();
            this.saveLog = new System.Windows.Forms.SaveFileDialog();
            this.SimSave = new System.Windows.Forms.SaveFileDialog();
            this.MainMenu.SuspendLayout();
            this.MainStatus.SuspendLayout();
            this.SolverPanel.SuspendLayout();
            this.gDynQuality.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gRailwayParams.SuspendLayout();
            this.gVehicleParams.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemFile,
            this.itemSim,
            this.itemAbout});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(1008, 24);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "MainMenu";
            // 
            // itemFile
            // 
            this.itemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subitemQuit});
            this.itemFile.Name = "itemFile";
            this.itemFile.Size = new System.Drawing.Size(37, 20);
            this.itemFile.Text = "File";
            // 
            // subitemQuit
            // 
            this.subitemQuit.Name = "subitemQuit";
            this.subitemQuit.Size = new System.Drawing.Size(97, 22);
            this.subitemQuit.Text = "Quit";
            this.subitemQuit.Click += new System.EventHandler(this.subitemQuit_Click);
            // 
            // itemSim
            // 
            this.itemSim.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subitemStart,
            this.subitemPause,
            this.subitemStop,
            this.toolStripSeparator1,
            this.subitemSave,
            this.subitemOptions});
            this.itemSim.Name = "itemSim";
            this.itemSim.Size = new System.Drawing.Size(76, 20);
            this.itemSim.Text = "Simulation";
            // 
            // subitemStart
            // 
            this.subitemStart.Name = "subitemStart";
            this.subitemStart.Size = new System.Drawing.Size(135, 22);
            this.subitemStart.Text = "Start";
            this.subitemStart.Click += new System.EventHandler(this.subitemStart_Click);
            // 
            // subitemPause
            // 
            this.subitemPause.Enabled = false;
            this.subitemPause.Name = "subitemPause";
            this.subitemPause.Size = new System.Drawing.Size(135, 22);
            this.subitemPause.Text = "Pause";
            this.subitemPause.Click += new System.EventHandler(this.subitemPause_Click);
            // 
            // subitemStop
            // 
            this.subitemStop.Enabled = false;
            this.subitemStop.Name = "subitemStop";
            this.subitemStop.Size = new System.Drawing.Size(135, 22);
            this.subitemStop.Text = "Stop";
            this.subitemStop.Click += new System.EventHandler(this.subitemStop_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(132, 6);
            // 
            // subitemSave
            // 
            this.subitemSave.Enabled = false;
            this.subitemSave.Name = "subitemSave";
            this.subitemSave.Size = new System.Drawing.Size(135, 22);
            this.subitemSave.Text = "Save results";
            this.subitemSave.Click += new System.EventHandler(this.subitemSave_Click);
            // 
            // subitemOptions
            // 
            this.subitemOptions.Name = "subitemOptions";
            this.subitemOptions.Size = new System.Drawing.Size(135, 22);
            this.subitemOptions.Text = "Options";
            this.subitemOptions.Click += new System.EventHandler(this.subitemOptions_Click);
            // 
            // itemAbout
            // 
            this.itemAbout.Name = "itemAbout";
            this.itemAbout.Size = new System.Drawing.Size(52, 20);
            this.itemAbout.Text = "About";
            this.itemAbout.Click += new System.EventHandler(this.itemAbout_Click);
            // 
            // MainStatus
            // 
            this.MainStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.solverStatus,
            this.optProgress});
            this.MainStatus.Location = new System.Drawing.Point(0, 707);
            this.MainStatus.Name = "MainStatus";
            this.MainStatus.Size = new System.Drawing.Size(1008, 22);
            this.MainStatus.TabIndex = 1;
            this.MainStatus.Text = "MainStatus";
            // 
            // solverStatus
            // 
            this.solverStatus.Name = "solverStatus";
            this.solverStatus.Size = new System.Drawing.Size(38, 17);
            this.solverStatus.Text = "status";
            // 
            // optProgress
            // 
            this.optProgress.Name = "optProgress";
            this.optProgress.Size = new System.Drawing.Size(100, 16);
            // 
            // SolverPanel
            // 
            this.SolverPanel.Controls.Add(this.gDynQuality);
            this.SolverPanel.Controls.Add(this.groupBox1);
            this.SolverPanel.Controls.Add(this.gRailwayParams);
            this.SolverPanel.Controls.Add(this.MainToolBar);
            this.SolverPanel.Controls.Add(this.gVehicleParams);
            this.SolverPanel.Controls.Add(this.LogBox);
            this.SolverPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SolverPanel.Location = new System.Drawing.Point(0, 24);
            this.SolverPanel.Name = "SolverPanel";
            this.SolverPanel.Size = new System.Drawing.Size(1008, 683);
            this.SolverPanel.TabIndex = 2;
            // 
            // gDynQuality
            // 
            this.gDynQuality.Controls.Add(this.changeSaveFile);
            this.gDynQuality.Controls.Add(this.dyncalcStop);
            this.gDynQuality.Controls.Add(this.dyncalcPause);
            this.gDynQuality.Controls.Add(this.dyncalcStart);
            this.gDynQuality.Controls.Add(this.label20);
            this.gDynQuality.Controls.Add(this.label19);
            this.gDynQuality.Controls.Add(this.label18);
            this.gDynQuality.Controls.Add(this.VelocityStep);
            this.gDynQuality.Controls.Add(this.MaxVelocity);
            this.gDynQuality.Controls.Add(this.InitVelocity);
            this.gDynQuality.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gDynQuality.Location = new System.Drawing.Point(667, 26);
            this.gDynQuality.Name = "gDynQuality";
            this.gDynQuality.Size = new System.Drawing.Size(329, 211);
            this.gDynQuality.TabIndex = 8;
            this.gDynQuality.TabStop = false;
            this.gDynQuality.Text = "Dynamic quality calculation";
            // 
            // changeSaveFile
            // 
            this.changeSaveFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.changeSaveFile.Location = new System.Drawing.Point(242, 157);
            this.changeSaveFile.Name = "changeSaveFile";
            this.changeSaveFile.Size = new System.Drawing.Size(27, 27);
            this.changeSaveFile.TabIndex = 46;
            this.changeSaveFile.Text = "...";
            this.changeSaveFile.UseVisualStyleBackColor = true;
            this.changeSaveFile.Click += new System.EventHandler(this.changeSaveFile_Click);
            // 
            // dyncalcStop
            // 
            this.dyncalcStop.Enabled = false;
            this.dyncalcStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dyncalcStop.Location = new System.Drawing.Point(166, 157);
            this.dyncalcStop.Name = "dyncalcStop";
            this.dyncalcStop.Size = new System.Drawing.Size(61, 27);
            this.dyncalcStop.TabIndex = 44;
            this.dyncalcStop.Text = "Stop";
            this.dyncalcStop.UseVisualStyleBackColor = true;
            this.dyncalcStop.Click += new System.EventHandler(this.dyncalcStop_Click);
            // 
            // dyncalcPause
            // 
            this.dyncalcPause.Enabled = false;
            this.dyncalcPause.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dyncalcPause.Location = new System.Drawing.Point(99, 157);
            this.dyncalcPause.Name = "dyncalcPause";
            this.dyncalcPause.Size = new System.Drawing.Size(61, 27);
            this.dyncalcPause.TabIndex = 43;
            this.dyncalcPause.Text = "Pause";
            this.dyncalcPause.UseVisualStyleBackColor = true;
            this.dyncalcPause.Click += new System.EventHandler(this.dyncalcPause_Click);
            // 
            // dyncalcStart
            // 
            this.dyncalcStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dyncalcStart.Location = new System.Drawing.Point(32, 157);
            this.dyncalcStart.Name = "dyncalcStart";
            this.dyncalcStart.Size = new System.Drawing.Size(61, 27);
            this.dyncalcStart.TabIndex = 23;
            this.dyncalcStart.Text = "Start";
            this.dyncalcStart.UseVisualStyleBackColor = true;
            this.dyncalcStart.Click += new System.EventHandler(this.dyncalcStart_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(29, 104);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(107, 15);
            this.label20.TabIndex = 22;
            this.label20.Text = "Velocity step, km/h";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(29, 71);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(130, 15);
            this.label19.TabIndex = 21;
            this.label19.Text = "Maximal velocity, km/h";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(29, 41);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(111, 15);
            this.label18.TabIndex = 20;
            this.label18.Text = "Initial velocity, km/h";
            // 
            // VelocityStep
            // 
            this.VelocityStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.VelocityStep.Location = new System.Drawing.Point(165, 103);
            this.VelocityStep.Name = "VelocityStep";
            this.VelocityStep.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.VelocityStep.Size = new System.Drawing.Size(55, 20);
            this.VelocityStep.TabIndex = 19;
            this.VelocityStep.Text = "1.0";
            // 
            // MaxVelocity
            // 
            this.MaxVelocity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaxVelocity.Location = new System.Drawing.Point(165, 71);
            this.MaxVelocity.Name = "MaxVelocity";
            this.MaxVelocity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MaxVelocity.Size = new System.Drawing.Size(55, 20);
            this.MaxVelocity.TabIndex = 18;
            this.MaxVelocity.Text = "600.0";
            // 
            // InitVelocity
            // 
            this.InitVelocity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InitVelocity.Location = new System.Drawing.Point(165, 41);
            this.InitVelocity.Name = "InitVelocity";
            this.InitVelocity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.InitVelocity.Size = new System.Drawing.Size(55, 20);
            this.InitVelocity.TabIndex = 17;
            this.InitVelocity.Text = "10.0";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.optSendParams);
            this.groupBox1.Controls.Add(this.optStop);
            this.groupBox1.Controls.Add(this.optPause);
            this.groupBox1.Controls.Add(this.Accuracy);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.Gamma2Max);
            this.groupBox1.Controls.Add(this.Gamma2Min);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.Gamma1Max);
            this.groupBox1.Controls.Add(this.Gamma1Min);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.Fs2Max);
            this.groupBox1.Controls.Add(this.Fs2Min);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.Fs1Max);
            this.groupBox1.Controls.Add(this.Fs1Min);
            this.groupBox1.Controls.Add(this.optStart);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(323, 151);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(338, 359);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Optimization";
            // 
            // optSendParams
            // 
            this.optSendParams.Enabled = false;
            this.optSendParams.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.optSendParams.Location = new System.Drawing.Point(20, 315);
            this.optSendParams.Name = "optSendParams";
            this.optSendParams.Size = new System.Drawing.Size(40, 27);
            this.optSendParams.TabIndex = 44;
            this.optSendParams.Text = "<---";
            this.optSendParams.UseVisualStyleBackColor = true;
            this.optSendParams.Click += new System.EventHandler(this.optSendParams_Click);
            // 
            // optStop
            // 
            this.optStop.Enabled = false;
            this.optStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.optStop.Location = new System.Drawing.Point(230, 315);
            this.optStop.Name = "optStop";
            this.optStop.Size = new System.Drawing.Size(61, 27);
            this.optStop.TabIndex = 43;
            this.optStop.Text = "Stop";
            this.optStop.UseVisualStyleBackColor = true;
            this.optStop.Click += new System.EventHandler(this.optStop_Click);
            // 
            // optPause
            // 
            this.optPause.Enabled = false;
            this.optPause.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.optPause.Location = new System.Drawing.Point(161, 315);
            this.optPause.Name = "optPause";
            this.optPause.Size = new System.Drawing.Size(61, 27);
            this.optPause.TabIndex = 42;
            this.optPause.Text = "Pause";
            this.optPause.UseVisualStyleBackColor = true;
            this.optPause.Click += new System.EventHandler(this.optPause_Click);
            // 
            // Accuracy
            // 
            this.Accuracy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Accuracy.Location = new System.Drawing.Point(163, 279);
            this.Accuracy.Name = "Accuracy";
            this.Accuracy.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Accuracy.Size = new System.Drawing.Size(75, 20);
            this.Accuracy.TabIndex = 41;
            this.Accuracy.Text = "1e-5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(99, 280);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 15);
            this.label7.TabIndex = 40;
            this.label7.Text = "Accuracy";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(17, 216);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(183, 15);
            this.label16.TabIndex = 39;
            this.label16.Text = "Stage #2 damp coeff range, mm";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(139, 243);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 15);
            this.label17.TabIndex = 38;
            this.label17.Text = "--";
            // 
            // Gamma2Max
            // 
            this.Gamma2Max.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Gamma2Max.Location = new System.Drawing.Point(161, 242);
            this.Gamma2Max.Name = "Gamma2Max";
            this.Gamma2Max.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Gamma2Max.Size = new System.Drawing.Size(75, 20);
            this.Gamma2Max.TabIndex = 36;
            this.Gamma2Max.Text = "1.0";
            // 
            // Gamma2Min
            // 
            this.Gamma2Min.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Gamma2Min.Location = new System.Drawing.Point(60, 243);
            this.Gamma2Min.Name = "Gamma2Min";
            this.Gamma2Min.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Gamma2Min.Size = new System.Drawing.Size(75, 20);
            this.Gamma2Min.TabIndex = 35;
            this.Gamma2Min.Text = "0.001";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(17, 159);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(183, 15);
            this.label13.TabIndex = 34;
            this.label13.Text = "Stage #1 damp coeff range, mm";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(139, 186);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 15);
            this.label14.TabIndex = 33;
            this.label14.Text = "--";
            // 
            // Gamma1Max
            // 
            this.Gamma1Max.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Gamma1Max.Location = new System.Drawing.Point(160, 185);
            this.Gamma1Max.Name = "Gamma1Max";
            this.Gamma1Max.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Gamma1Max.Size = new System.Drawing.Size(75, 20);
            this.Gamma1Max.TabIndex = 31;
            this.Gamma1Max.Text = "1.0";
            // 
            // Gamma1Min
            // 
            this.Gamma1Min.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Gamma1Min.Location = new System.Drawing.Point(60, 186);
            this.Gamma1Min.Name = "Gamma1Min";
            this.Gamma1Min.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Gamma1Min.Size = new System.Drawing.Size(75, 20);
            this.Gamma1Min.TabIndex = 30;
            this.Gamma1Min.Text = "0.001";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(17, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(219, 15);
            this.label10.TabIndex = 29;
            this.label10.Text = "Stage #2 static deformation range, mm";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(139, 127);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(15, 15);
            this.label11.TabIndex = 28;
            this.label11.Text = "--";
            // 
            // Fs2Max
            // 
            this.Fs2Max.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Fs2Max.Location = new System.Drawing.Point(161, 126);
            this.Fs2Max.Name = "Fs2Max";
            this.Fs2Max.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Fs2Max.Size = new System.Drawing.Size(75, 20);
            this.Fs2Max.TabIndex = 26;
            this.Fs2Max.Text = "125.0";
            // 
            // Fs2Min
            // 
            this.Fs2Min.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Fs2Min.Location = new System.Drawing.Point(60, 127);
            this.Fs2Min.Name = "Fs2Min";
            this.Fs2Min.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Fs2Min.Size = new System.Drawing.Size(75, 20);
            this.Fs2Min.TabIndex = 25;
            this.Fs2Min.Text = "50.0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(17, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(219, 15);
            this.label9.TabIndex = 24;
            this.label9.Text = "Stage #1 static deformation range, mm";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(139, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 15);
            this.label8.TabIndex = 23;
            this.label8.Text = "--";
            // 
            // Fs1Max
            // 
            this.Fs1Max.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Fs1Max.Location = new System.Drawing.Point(161, 70);
            this.Fs1Max.Name = "Fs1Max";
            this.Fs1Max.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Fs1Max.Size = new System.Drawing.Size(75, 20);
            this.Fs1Max.TabIndex = 16;
            this.Fs1Max.Text = "125.0";
            // 
            // Fs1Min
            // 
            this.Fs1Min.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Fs1Min.Location = new System.Drawing.Point(60, 71);
            this.Fs1Min.Name = "Fs1Min";
            this.Fs1Min.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Fs1Min.Size = new System.Drawing.Size(75, 20);
            this.Fs1Min.TabIndex = 15;
            this.Fs1Min.Text = "50.0";
            // 
            // optStart
            // 
            this.optStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.optStart.Location = new System.Drawing.Point(93, 315);
            this.optStart.Name = "optStart";
            this.optStart.Size = new System.Drawing.Size(61, 27);
            this.optStart.TabIndex = 0;
            this.optStart.Text = "Start";
            this.optStart.UseVisualStyleBackColor = true;
            this.optStart.Click += new System.EventHandler(this.optStart_Click);
            // 
            // gRailwayParams
            // 
            this.gRailwayParams.Controls.Add(this.RandomLaw);
            this.gRailwayParams.Controls.Add(this.label6);
            this.gRailwayParams.Controls.Add(this.label5);
            this.gRailwayParams.Controls.Add(this.Amplify);
            this.gRailwayParams.Controls.Add(this.WaveLength);
            this.gRailwayParams.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gRailwayParams.Location = new System.Drawing.Point(323, 26);
            this.gRailwayParams.Name = "gRailwayParams";
            this.gRailwayParams.Size = new System.Drawing.Size(338, 119);
            this.gRailwayParams.TabIndex = 5;
            this.gRailwayParams.TabStop = false;
            this.gRailwayParams.Text = "Unevenness of railway track";
            // 
            // RandomLaw
            // 
            this.RandomLaw.AutoSize = true;
            this.RandomLaw.Enabled = false;
            this.RandomLaw.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RandomLaw.Location = new System.Drawing.Point(20, 79);
            this.RandomLaw.Name = "RandomLaw";
            this.RandomLaw.Size = new System.Drawing.Size(218, 20);
            this.RandomLaw.TabIndex = 19;
            this.RandomLaw.Text = "use ramdom law of unevenness ";
            this.RandomLaw.UseVisualStyleBackColor = true;
            this.RandomLaw.CheckedChanged += new System.EventHandler(this.RandomLaw_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(183, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 15);
            this.label6.TabIndex = 18;
            this.label6.Text = "Amplify, mm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(17, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 15);
            this.label5.TabIndex = 17;
            this.label5.Text = "Wave length, m";
            // 
            // Amplify
            // 
            this.Amplify.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Amplify.Location = new System.Drawing.Point(263, 40);
            this.Amplify.Name = "Amplify";
            this.Amplify.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Amplify.Size = new System.Drawing.Size(55, 20);
            this.Amplify.TabIndex = 16;
            this.Amplify.Text = "5.0";
            // 
            // WaveLength
            // 
            this.WaveLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WaveLength.Location = new System.Drawing.Point(122, 41);
            this.WaveLength.Name = "WaveLength";
            this.WaveLength.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.WaveLength.Size = new System.Drawing.Size(55, 20);
            this.WaveLength.TabIndex = 15;
            this.WaveLength.Text = "25.0";
            // 
            // MainToolBar
            // 
            this.MainToolBar.Location = new System.Drawing.Point(0, 0);
            this.MainToolBar.Name = "MainToolBar";
            this.MainToolBar.Size = new System.Drawing.Size(1008, 25);
            this.MainToolBar.TabIndex = 4;
            this.MainToolBar.Text = "toolStrip1";
            // 
            // gVehicleParams
            // 
            this.gVehicleParams.Controls.Add(this.label4);
            this.gVehicleParams.Controls.Add(this.label3);
            this.gVehicleParams.Controls.Add(this.label2);
            this.gVehicleParams.Controls.Add(this.label1);
            this.gVehicleParams.Controls.Add(this.LSpringsParams);
            this.gVehicleParams.Controls.Add(this.Stage2DampCoeff);
            this.gVehicleParams.Controls.Add(this.Stage1DampCoeff);
            this.gVehicleParams.Controls.Add(this.Stage2StaticDeformation);
            this.gVehicleParams.Controls.Add(this.Stage1StaticDeformation);
            this.gVehicleParams.Controls.Add(this.LVelocity);
            this.gVehicleParams.Controls.Add(this.LTrolleyHeight);
            this.gVehicleParams.Controls.Add(this.LTrolleyBase);
            this.gVehicleParams.Controls.Add(this.LBodyHeight);
            this.gVehicleParams.Controls.Add(this.LBodyBase);
            this.gVehicleParams.Controls.Add(this.LTrolleyCoeff);
            this.gVehicleParams.Controls.Add(this.LBodyMass);
            this.gVehicleParams.Controls.Add(this.ConstructVelocity);
            this.gVehicleParams.Controls.Add(this.TrolleyHeight);
            this.gVehicleParams.Controls.Add(this.BodyHeight);
            this.gVehicleParams.Controls.Add(this.TrolleyBase);
            this.gVehicleParams.Controls.Add(this.BodyBase);
            this.gVehicleParams.Controls.Add(this.TrolleyCoeff);
            this.gVehicleParams.Controls.Add(this.VehicleMass);
            this.gVehicleParams.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gVehicleParams.Location = new System.Drawing.Point(12, 26);
            this.gVehicleParams.Name = "gVehicleParams";
            this.gVehicleParams.Size = new System.Drawing.Size(305, 484);
            this.gVehicleParams.TabIndex = 3;
            this.gVehicleParams.TabStop = false;
            this.gVehicleParams.Text = "Vehicle parametres";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(22, 444);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 15);
            this.label4.TabIndex = 22;
            this.label4.Text = "Stage #2 damp coeff";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(22, 408);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 15);
            this.label3.TabIndex = 21;
            this.label3.Text = "Stage #1 damp coeff";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(22, 371);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 15);
            this.label2.TabIndex = 20;
            this.label2.Text = "Stage #2 static deformation, mm";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(22, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 15);
            this.label1.TabIndex = 19;
            this.label1.Text = "Stage #1 static deformation, mm";
            // 
            // LSpringsParams
            // 
            this.LSpringsParams.AutoSize = true;
            this.LSpringsParams.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LSpringsParams.Location = new System.Drawing.Point(21, 295);
            this.LSpringsParams.Name = "LSpringsParams";
            this.LSpringsParams.Size = new System.Drawing.Size(218, 20);
            this.LSpringsParams.TabIndex = 18;
            this.LSpringsParams.Text = "Stages of springs parameters";
            // 
            // Stage2DampCoeff
            // 
            this.Stage2DampCoeff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Stage2DampCoeff.Location = new System.Drawing.Point(212, 444);
            this.Stage2DampCoeff.Name = "Stage2DampCoeff";
            this.Stage2DampCoeff.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Stage2DampCoeff.Size = new System.Drawing.Size(75, 20);
            this.Stage2DampCoeff.TabIndex = 17;
            this.Stage2DampCoeff.Text = "0.1";
            // 
            // Stage1DampCoeff
            // 
            this.Stage1DampCoeff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Stage1DampCoeff.Location = new System.Drawing.Point(212, 408);
            this.Stage1DampCoeff.Name = "Stage1DampCoeff";
            this.Stage1DampCoeff.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Stage1DampCoeff.Size = new System.Drawing.Size(75, 20);
            this.Stage1DampCoeff.TabIndex = 16;
            this.Stage1DampCoeff.Text = "0.1";
            // 
            // Stage2StaticDeformation
            // 
            this.Stage2StaticDeformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Stage2StaticDeformation.Location = new System.Drawing.Point(212, 371);
            this.Stage2StaticDeformation.Name = "Stage2StaticDeformation";
            this.Stage2StaticDeformation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Stage2StaticDeformation.Size = new System.Drawing.Size(75, 20);
            this.Stage2StaticDeformation.TabIndex = 15;
            this.Stage2StaticDeformation.Text = "125.0";
            // 
            // Stage1StaticDeformation
            // 
            this.Stage1StaticDeformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Stage1StaticDeformation.Location = new System.Drawing.Point(212, 336);
            this.Stage1StaticDeformation.Name = "Stage1StaticDeformation";
            this.Stage1StaticDeformation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Stage1StaticDeformation.Size = new System.Drawing.Size(75, 20);
            this.Stage1StaticDeformation.TabIndex = 14;
            this.Stage1StaticDeformation.Text = "125.0";
            // 
            // LVelocity
            // 
            this.LVelocity.AutoSize = true;
            this.LVelocity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LVelocity.Location = new System.Drawing.Point(42, 252);
            this.LVelocity.Name = "LVelocity";
            this.LVelocity.Size = new System.Drawing.Size(133, 15);
            this.LVelocity.TabIndex = 13;
            this.LVelocity.Text = "Construct velocity, km/h";
            // 
            // LTrolleyHeight
            // 
            this.LTrolleyHeight.AutoSize = true;
            this.LTrolleyHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LTrolleyHeight.Location = new System.Drawing.Point(42, 217);
            this.LTrolleyHeight.Name = "LTrolleyHeight";
            this.LTrolleyHeight.Size = new System.Drawing.Size(97, 15);
            this.LTrolleyHeight.TabIndex = 12;
            this.LTrolleyHeight.Text = "Trolley height, m";
            // 
            // LTrolleyBase
            // 
            this.LTrolleyBase.AutoSize = true;
            this.LTrolleyBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LTrolleyBase.Location = new System.Drawing.Point(42, 178);
            this.LTrolleyBase.Name = "LTrolleyBase";
            this.LTrolleyBase.Size = new System.Drawing.Size(117, 15);
            this.LTrolleyBase.TabIndex = 11;
            this.LTrolleyBase.Text = "Trolley rigid base, m";
            // 
            // LBodyHeight
            // 
            this.LBodyHeight.AutoSize = true;
            this.LBodyHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LBodyHeight.Location = new System.Drawing.Point(42, 141);
            this.LBodyHeight.Name = "LBodyHeight";
            this.LBodyHeight.Size = new System.Drawing.Size(88, 15);
            this.LBodyHeight.TabIndex = 10;
            this.LBodyHeight.Text = "Body height, m";
            // 
            // LBodyBase
            // 
            this.LBodyBase.AutoSize = true;
            this.LBodyBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LBodyBase.Location = new System.Drawing.Point(42, 104);
            this.LBodyBase.Name = "LBodyBase";
            this.LBodyBase.Size = new System.Drawing.Size(108, 15);
            this.LBodyBase.TabIndex = 9;
            this.LBodyBase.Text = "Body rigid base, m";
            // 
            // LTrolleyCoeff
            // 
            this.LTrolleyCoeff.AutoSize = true;
            this.LTrolleyCoeff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LTrolleyCoeff.Location = new System.Drawing.Point(42, 66);
            this.LTrolleyCoeff.Name = "LTrolleyCoeff";
            this.LTrolleyCoeff.Size = new System.Drawing.Size(105, 15);
            this.LTrolleyCoeff.TabIndex = 8;
            this.LTrolleyCoeff.Text = "Trolley mass coeff";
            // 
            // LBodyMass
            // 
            this.LBodyMass.AutoSize = true;
            this.LBodyMass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LBodyMass.Location = new System.Drawing.Point(42, 31);
            this.LBodyMass.Name = "LBodyMass";
            this.LBodyMass.Size = new System.Drawing.Size(99, 15);
            this.LBodyMass.TabIndex = 7;
            this.LBodyMass.Text = "Vehicle mass, kg";
            // 
            // ConstructVelocity
            // 
            this.ConstructVelocity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConstructVelocity.Location = new System.Drawing.Point(187, 252);
            this.ConstructVelocity.Name = "ConstructVelocity";
            this.ConstructVelocity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ConstructVelocity.Size = new System.Drawing.Size(100, 20);
            this.ConstructVelocity.TabIndex = 6;
            this.ConstructVelocity.Text = "250.0";
            // 
            // TrolleyHeight
            // 
            this.TrolleyHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TrolleyHeight.Location = new System.Drawing.Point(187, 216);
            this.TrolleyHeight.Name = "TrolleyHeight";
            this.TrolleyHeight.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TrolleyHeight.Size = new System.Drawing.Size(100, 20);
            this.TrolleyHeight.TabIndex = 5;
            this.TrolleyHeight.Text = "0.5";
            // 
            // BodyHeight
            // 
            this.BodyHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BodyHeight.Location = new System.Drawing.Point(187, 141);
            this.BodyHeight.Name = "BodyHeight";
            this.BodyHeight.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BodyHeight.Size = new System.Drawing.Size(100, 20);
            this.BodyHeight.TabIndex = 4;
            this.BodyHeight.Text = "2.5";
            // 
            // TrolleyBase
            // 
            this.TrolleyBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TrolleyBase.Location = new System.Drawing.Point(187, 178);
            this.TrolleyBase.Name = "TrolleyBase";
            this.TrolleyBase.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TrolleyBase.Size = new System.Drawing.Size(100, 20);
            this.TrolleyBase.TabIndex = 3;
            this.TrolleyBase.Text = "3.0";
            // 
            // BodyBase
            // 
            this.BodyBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BodyBase.Location = new System.Drawing.Point(187, 104);
            this.BodyBase.Name = "BodyBase";
            this.BodyBase.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BodyBase.Size = new System.Drawing.Size(100, 20);
            this.BodyBase.TabIndex = 2;
            this.BodyBase.Text = "20.0";
            // 
            // TrolleyCoeff
            // 
            this.TrolleyCoeff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TrolleyCoeff.Location = new System.Drawing.Point(187, 66);
            this.TrolleyCoeff.Name = "TrolleyCoeff";
            this.TrolleyCoeff.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TrolleyCoeff.Size = new System.Drawing.Size(100, 20);
            this.TrolleyCoeff.TabIndex = 1;
            this.TrolleyCoeff.Text = "0.4";
            // 
            // VehicleMass
            // 
            this.VehicleMass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.VehicleMass.Location = new System.Drawing.Point(187, 31);
            this.VehicleMass.Name = "VehicleMass";
            this.VehicleMass.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.VehicleMass.Size = new System.Drawing.Size(100, 20);
            this.VehicleMass.TabIndex = 0;
            this.VehicleMass.Text = "65000.0";
            // 
            // LogBox
            // 
            this.LogBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.LogBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LogBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogBox.Location = new System.Drawing.Point(0, 522);
            this.LogBox.Name = "LogBox";
            this.LogBox.Size = new System.Drawing.Size(1008, 161);
            this.LogBox.TabIndex = 1;
            this.LogBox.Text = "";
            this.LogBox.TextChanged += new System.EventHandler(this.LogBox_TextChanged);
            this.LogBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.LogBox_MouseUp);
            // 
            // savePDKlog
            // 
            this.savePDKlog.DefaultExt = "csv";
            // 
            // saveLog
            // 
            this.saveLog.DefaultExt = "txt";
            this.saveLog.Filter = "text file (*.txt)|*.txt";
            // 
            // SimSave
            // 
            this.SimSave.DefaultExt = "csv";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.SolverPanel);
            this.Controls.Add(this.MainStatus);
            this.Controls.Add(this.MainMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MainMenu;
            this.Name = "MainForm";
            this.Text = "Vertical Dynamics";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.MainStatus.ResumeLayout(false);
            this.MainStatus.PerformLayout();
            this.SolverPanel.ResumeLayout(false);
            this.SolverPanel.PerformLayout();
            this.gDynQuality.ResumeLayout(false);
            this.gDynQuality.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gRailwayParams.ResumeLayout(false);
            this.gRailwayParams.PerformLayout();
            this.gVehicleParams.ResumeLayout(false);
            this.gVehicleParams.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem itemFile;
        private System.Windows.Forms.ToolStripMenuItem subitemQuit;
        private System.Windows.Forms.StatusStrip MainStatus;
        private System.Windows.Forms.Panel SolverPanel;       
        private System.Windows.Forms.RichTextBox LogBox;
        private System.Windows.Forms.ToolStripMenuItem itemSim;
        private System.Windows.Forms.ToolStripMenuItem subitemStart;
        private System.Windows.Forms.ToolStripMenuItem subitemPause;
        private System.Windows.Forms.ToolStripMenuItem subitemStop;
        private System.Windows.Forms.GroupBox gVehicleParams;
        private System.Windows.Forms.ToolStrip MainToolBar;
        private System.Windows.Forms.TextBox VehicleMass;
        private System.Windows.Forms.TextBox TrolleyHeight;
        private System.Windows.Forms.TextBox BodyHeight;
        private System.Windows.Forms.TextBox TrolleyBase;
        private System.Windows.Forms.TextBox BodyBase;
        private System.Windows.Forms.TextBox TrolleyCoeff;
        private System.Windows.Forms.TextBox ConstructVelocity;
        private System.Windows.Forms.Label LBodyMass;
        private System.Windows.Forms.Label LVelocity;
        private System.Windows.Forms.Label LTrolleyHeight;
        private System.Windows.Forms.Label LTrolleyBase;
        private System.Windows.Forms.Label LBodyHeight;
        private System.Windows.Forms.Label LBodyBase;
        private System.Windows.Forms.Label LTrolleyCoeff;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LSpringsParams;
        private System.Windows.Forms.TextBox Stage2DampCoeff;
        private System.Windows.Forms.TextBox Stage1DampCoeff;
        private System.Windows.Forms.TextBox Stage2StaticDeformation;
        private System.Windows.Forms.TextBox Stage1StaticDeformation;
        private System.Windows.Forms.GroupBox gRailwayParams;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Amplify;
        private System.Windows.Forms.TextBox WaveLength;
        private System.Windows.Forms.CheckBox RandomLaw;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button optStart;
        private System.Windows.Forms.ToolStripStatusLabel solverStatus;
        private System.Windows.Forms.ToolStripProgressBar optProgress;
        private System.Windows.Forms.TextBox Accuracy;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Gamma2Max;
        private System.Windows.Forms.TextBox Gamma2Min;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Gamma1Max;
        private System.Windows.Forms.TextBox Gamma1Min;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Fs2Max;
        private System.Windows.Forms.TextBox Fs2Min;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Fs1Max;
        private System.Windows.Forms.TextBox Fs1Min;
        private System.Windows.Forms.Button optStop;
        private System.Windows.Forms.Button optPause;
        private System.Windows.Forms.Button optSendParams;
        private System.Windows.Forms.GroupBox gDynQuality;
        private System.Windows.Forms.TextBox MaxVelocity;
        private System.Windows.Forms.TextBox InitVelocity;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox VelocityStep;
        private System.Windows.Forms.Button dyncalcStart;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button dyncalcStop;
        private System.Windows.Forms.Button dyncalcPause;
        private System.Windows.Forms.SaveFileDialog savePDKlog;
        private System.Windows.Forms.Button changeSaveFile;
        private System.Windows.Forms.SaveFileDialog saveLog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem subitemSave;
        private System.Windows.Forms.ToolStripMenuItem subitemOptions;
        private System.Windows.Forms.SaveFileDialog SimSave;
        private System.Windows.Forms.ToolStripMenuItem itemAbout;
    }
}

