﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Reflection;
using System.Xml;
using System.Globalization;
using System.IO;
using System.Diagnostics;

using core;

namespace solver
{
    public partial class MainForm : Form
    {
        // Some usefull constants 
        private const string RESULTS_FOLDER = "../results/";
        private const string GNUPLOT_FOLDER = "gnuplot\\bin\\";
        private const double g = 9.81;        

        // For model initialization from DLL
        Model model = null;
        Simulator simulator = null;
        Solver method;
        SolverData solver_data;
        bool init_ok = true;
        
        // Controls for simulation thread
        EventWaitHandle sim_ew;
        bool sim_stop;
        bool sim_pause;

        // Controls for optimization thread
        EventWaitHandle opt_ew;
        bool opt_stop;
        bool opt_pause;       

        // Constorls for dynamics calculation
        EventWaitHandle dyncalc_ew;
        bool dyncalc_stop;
        bool dyncalc_pause;

        
        LogPrinter log_printer;
        LogPrinter sim_printer;
        LogPrinter log_springs;

        // Vehicle params
        double m2;
        double m1;
        double a;
        double Lb;
        double I1;
        double I2;        
        double fs1;
        double fs2;

        // List of springs data int time space
        List<SpringData> springs_data_list;
        List<SimData> sim_data_list;

        // Data for optimization (limits)
        OptData opt_data;

        Form options;
        Form about;

        //---------------------------------------------------------------------
        //      Main form constructor
        //---------------------------------------------------------------------
        public MainForm()
        {
            InitializeComponent();

            // Init model
            init_ok = init();

            if (!init_ok)
                log_print("Initialization is failed\n");
        }

        //---------------------------------------------------------------------
        //      Exit from application
        //---------------------------------------------------------------------
        private void subitemQuit_Click(object sender, EventArgs e)
        {
            quit();
        }

        //---------------------------------------------------------------------
        //      Correct application quit
        //---------------------------------------------------------------------
        private void quit()
        {
            if (!sim_stop)
                sim_stop = true;

            if (!opt_stop)
                opt_stop = true;

            Application.Exit();
        }

        //---------------------------------------------------------------------
        //      Solver initialization
        //---------------------------------------------------------------------
        public bool init()
        {
            // Read solver configuration
            solver_data = ReadConfig("../cfg/config.xml");

            // Creation of model object
            model = new Model();
            ModelStatus status = model.load(solver_data.model_dll);

            log_print(status.msg);

            if (!status.ready)
                 return false;

            // Get model dimention
            int n = model.get_dimention();            

            // Creation of simulator object
            simulator = new Simulator();

            // Configure simulator
            simulator.init(n);

            // Set integration method params
            set_simulation_params(solver_data);

            simulator.set_ode_system(model.dFdt);
            simulator.set_pre_step(model.pre_step);
            simulator.set_post_step(model.post_step);

            simulator.set_method(method.step);

            // Set object from information display
            log_printer = new LogPrinter();            
            log_printer.print_func = this.print_func;

            log_springs = new LogPrinter();
            log_springs.print_func = this.log_forces_func;

            sim_printer = new LogPrinter();
            sim_printer.print_func = this.sim_printer_func;

            // Set events for threads control
            sim_ew = new EventWaitHandle(false, EventResetMode.AutoReset);
            sim_stop = true;

            opt_ew = new EventWaitHandle(false, EventResetMode.AutoReset);
            opt_stop = true;

            dyncalc_ew = new EventWaitHandle(false, EventResetMode.AutoReset);
            dyncalc_stop = true;

            springs_data_list = new List<SpringData>();
            sim_data_list = new List<SimData>();

            opt_data = new OptData();

            string cur_dir = Directory.GetCurrentDirectory();            
            savePDKlog.Filter = "text file (*.csv)|*csv";
            savePDKlog.FileName = cur_dir.Replace("bin", "results") + "\\pdk.csv";

            SimSave.Filter = "text file (*.csv)|*csv";
            SimSave.FileName = cur_dir.Replace("bin", "results") + "\\simdata.csv";

            return true;
        }

        //---------------------------------------------------------------------
        //      Set simulation parameters
        //---------------------------------------------------------------------
        void set_simulation_params(SolverData solver_data)
        {
            simulator.set_init_time(solver_data.t_begin);
            simulator.set_end_time(solver_data.t_end);
            simulator.set_time_step(solver_data.max_step);
            simulator.set_local_error(solver_data.local_err);

            set_integration_method(solver_data);
        }

        private void set_integration_method(SolverData solver_data)
        {
            if (solver_data.method == "rk4")
                method = (Solver)new RK4Solver();

            if (solver_data.method == "euler")
                method = (Solver)new EulerSolver();

            if (solver_data.method == "rkf5")
                method = (Solver)new RKF5Solver();
        }

        //---------------------------------------------------------------------
        //      Print messages to windiw log
        //---------------------------------------------------------------------
        void log_print(string msg)
        {
            if (LogBox.InvokeRequired)
                LogBox.Invoke(new Action<string>((s) => LogBox.Text += s), msg + "\n");
            else
                LogBox.Text += msg + "\n";                     
        }

        //---------------------------------------------------------------------
        //      Read configuration from XML
        //---------------------------------------------------------------------
        SolverData ReadConfig(string cfg_path)
        {
            SolverData solver_data = new SolverData();

            XmlDocument doc = new XmlDocument();
            doc.Load(cfg_path);

            var nodes = doc.GetElementsByTagName("Solver");

            foreach (XmlNode node in nodes)
            {              
                foreach (XmlNode subnode in node.ChildNodes)
                {
                    
                    if (subnode.Name == "ModelDLL")
                    {
                        solver_data.model_dll = subnode.InnerText;
                    }

                    if (subnode.Name == "method")
                    {
                        solver_data.method = subnode.InnerText;
                    }

                    if (subnode.Name == "t_begin")
                    {
                        solver_data.t_begin = double.Parse(subnode.InnerText, CultureInfo.InvariantCulture);
                    }

                    if (subnode.Name == "t_end")
                    {
                        solver_data.t_end = double.Parse(subnode.InnerText, CultureInfo.InvariantCulture);
                    }

                    if (subnode.Name == "max_step")
                    {
                        solver_data.max_step = double.Parse(subnode.InnerText, CultureInfo.InvariantCulture);
                    }

                    if (subnode.Name == "local_err")
                    {
                        solver_data.local_err = double.Parse(subnode.InnerText, CultureInfo.InvariantCulture);
                    }

                    if (subnode.Name == "opt_sim_time")
                    {
                        solver_data.opt_sim_time = double.Parse(subnode.InnerText, CultureInfo.InvariantCulture);
                    }

                    if (subnode.Name == "points_num")
                    {
                        solver_data.points_num = int.Parse(subnode.InnerText, CultureInfo.InvariantCulture);
                    }

                    if (subnode.Name == "points_to_simlog")
                    {
                        solver_data.points_to_simlog = int.Parse(subnode.InnerText, CultureInfo.InvariantCulture);
                    }
                }
            }

            return solver_data;
        }

        //---------------------------------------------------------------------
        //      Get data from textbox with try exception 
        //---------------------------------------------------------------------
        bool get_texbox_double(TextBox textbox, ref double value)
        {
            bool flag = true;

            try
            {
                value = double.Parse(textbox.Text, CultureInfo.InvariantCulture);
                flag = true;
            }
            catch (Exception ex)
            {
                string msg = "FAIL: No digit value in " + textbox.Name.ToString();
                log_print(msg);
                log_print(ex.Message.ToString());
                flag = false;
            }

            return flag;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        bool get_texbox_int(TextBox textbox, ref int value)
        {
            bool flag = true;

            try
            {
                value = int.Parse(textbox.Text, CultureInfo.InvariantCulture);
                flag = true;
            }
            catch (Exception ex)
            {
                string msg = "FAIL: No digit value in " + textbox.Name.ToString();
                log_print(msg);
                log_print(ex.Message.ToString());
                flag = false;
            }

            return flag;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        bool get_vehicle_data(ref VehicleData vehicle_data)
        {
            if (!get_texbox_double(VehicleMass, ref vehicle_data.mass))
                return false;

            if (vehicle_data.mass <= 0)
            {
                log_print("ERROR: Vehicle mass is negative or zero");
                return false;
            }

            if (!get_texbox_double(TrolleyCoeff, ref vehicle_data.trolley_coeff))
                return false;

            if (vehicle_data.trolley_coeff <= 0)
            {
                log_print("ERROR: Trolley coeff is negative or zero");
                return false;
            }

            if (!get_texbox_double(BodyBase, ref vehicle_data.body_base))
                return false;

            if (vehicle_data.body_base <= 0)
            {
                log_print("ERROR: Body is negative or zero");
                return false;
            }

            if (!get_texbox_double(BodyHeight, ref vehicle_data.body_height))
                return false;

            if (vehicle_data.body_height <= 0)
            {
                log_print("ERROR: Body height is negative or zero");
                return false;
            }

            if (!get_texbox_double(TrolleyBase, ref vehicle_data.trolley_base))
                return false;

            if (vehicle_data.trolley_base <= 0)
            {
                log_print("ERROR: Trolley base is negative or zero");
                return false;
            }

            if (!get_texbox_double(TrolleyHeight, ref vehicle_data.trolley_height))
                return false;

            if (vehicle_data.trolley_height <= 0)
            {
                log_print("ERROR: Trolley height is negative or zero");
                return false;
            }

            if (!get_texbox_double(ConstructVelocity, ref vehicle_data.construct_velocity))
                return false;

            if (vehicle_data.construct_velocity <= 0)
            {
                log_print("ERROR: Construct velocity is negative or zero");
                return false;
            }

            if (!get_texbox_double(Stage1StaticDeformation, ref vehicle_data.fs1))
                return false;

            if (vehicle_data.fs1 <= 0)
            {
                log_print("ERROR: Static deformation #1 is negative or zero");
                return false;
            }

            if (!get_texbox_double(Stage2StaticDeformation, ref vehicle_data.fs2))
                return false;

            if ( vehicle_data.fs2 <= 0 ) 
            {
                log_print("ERROR: Static deformation #2 is negative or zero");
                return false;
            }

            if (!get_texbox_double(Stage1DampCoeff, ref vehicle_data.gamma1))
                return false;

            if (vehicle_data.gamma1 <= 0)
            {
                log_print("ERROR: Stage #1 damp coeff is negative or zero");
                return false;
            }

            if (!get_texbox_double(Stage2DampCoeff, ref vehicle_data.gamma2))
                return false;

            if (vehicle_data.gamma2 <= 0)
            {
                log_print("ERROR: Stage #2 damp coeff is negative or zero");
                return false;
            }

            return true;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private bool check_init()
        {
            // Check is correct initialization
            if (!init_ok)
            {
                log_print("Initialization is failed. Simulation is impossible");
                return false; 
            }

            return true;
        }        

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void subitemStart_Click(object sender, EventArgs e)
        {
            if (!check_init())
                return;
            
            // Get data from text fields
            VehicleData vehicle_data = new VehicleData();
            RailwayData railway_data = new RailwayData();

            if (!get_vehicle_data(ref vehicle_data))
            {
                log_print("FAIL: Vehicle data are incorrect. Simulation is impossible");
                return;
            }

            if (!get_railway_data(ref railway_data))
            {
                log_print("FAIL: Vehicle data are incorrect. Simulation is impossible");
                return;
            }

            // Send data to model
            send_data_to_model(vehicle_data, railway_data);

            // Model initialization
            model.init();            
            
            // Start simulation thread
            if (sim_stop)
            {
                subitemStart.Enabled = false;
                subitemPause.Enabled = subitemStop.Enabled = true;
                sim_stop = false;

                ThreadPool.QueueUserWorkItem(new WaitCallback((s) =>
                {
                   sim_thread_func();
                }));
            }
            else
            {
                sim_ew.Set();
                subitemStop.Enabled = subitemPause.Enabled = true;
                subitemStart.Enabled = false;
            }                       
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void send_data_to_model(VehicleData vehicle_data, RailwayData railway_data)
        {
            send_vehicle_data(vehicle_data);
            send_railway_data(railway_data);

            fs1 = vehicle_data.fs1 / 1000.0;
            fs2 = vehicle_data.fs2 /1000.0;
            

            model.set_stage1_damp_coeff(vehicle_data.gamma1);
            model.set_stage2_damp_coeff(vehicle_data.gamma2);
            model.set_stage1_static_deformation(fs1);
            model.set_stage2_static_deformation(fs2);

            model.set_velocity(vehicle_data.construct_velocity / 3.6);                             
        }

        private void send_vehicle_data(VehicleData vehicle_data)
        {
            m2 = 2*vehicle_data.mass / (2 + vehicle_data.trolley_coeff);
            m1 = (vehicle_data.mass - m2) / 2;
            a = vehicle_data.trolley_base / 2;
            Lb = vehicle_data.body_base / 2;
            I1 = m1 * (a * a + vehicle_data.trolley_height * vehicle_data.trolley_height) / 12;
            I2 = m2 * (Lb * Lb + vehicle_data.body_height * vehicle_data.body_height) / 12;

            model.set_body_mass(m2);
            model.set_trolley_mass(m1);
            model.set_trolley_half_base(a);
            model.set_body_half_base(Lb);
            model.set_trolley_moment_of_inertia(I1);
            model.set_body_moment_of_inertia(I2);
        }

        public void send_railway_data(RailwayData railway_data)
        {
            model.set_wave_lenght(railway_data.wave_length);
            model.set_amplify(railway_data.amplify / 1000.0);
            model.set_random_law(railway_data.random_law);
        }


        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void RandomLaw_CheckedChanged(object sender, EventArgs e)
        {
            if (RandomLaw.Checked)
            {
                WaveLength.Enabled = false;
                Amplify.Enabled = false;
            }
            else
            {
                WaveLength.Enabled = true;
                Amplify.Enabled = true;
            }
        }


        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private bool get_railway_data(ref RailwayData railway_data)
        {
            bool flag = true;

            if (!get_texbox_double(WaveLength, ref railway_data.wave_length))
                return false;

            if (railway_data.wave_length <= 0)
            {
                log_print("ERROR: Wave length is negative or zero");
                return false;
            }

            if (!get_texbox_double(Amplify, ref railway_data.amplify))
                return false;

            if (railway_data.amplify <= 0)
            {
                log_print("ERROR: Wave length is negative or zero");
                return false;
            }

            railway_data.random_law = RandomLaw.Checked;            

            return flag;
        }


        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void sim_thread_func()
        {
            // Reset simulator
            simulator.reset();

            // Set step quality flag
            bool is_correct = true;

            // Set log file
            //LogFile log_file = new LogFile("debug.csv");
            //log_file.print_func = this.log_print_func;

            //springs_data_list.Clear();
            sim_data_list.Clear();

            double DeltaT = solver_data.t_end / solver_data.points_to_simlog;

            if (DeltaT < solver_data.max_step)
                DeltaT = solver_data.max_step;

            // Main simulation loop
            while ( (simulator.get_time() <= simulator.get_end_time()) && is_correct && (!sim_stop) )
            {
                // Check is puase
                if (sim_pause)
                {
                    sim_ew.WaitOne();
                    sim_pause = false;
                }

                // Write to logbox
                log_printer.process(simulator.get_step(), 1.0);                                
                sim_printer.process(simulator.get_step(), DeltaT);
                // Simulation step
                is_correct = simulator.process();
            }

            sim_stop = true;
            
            set_subitem_enable(subitemStart, true);
            set_subitem_enable(subitemPause, false);
            set_subitem_enable(subitemStop, false);
            set_subitem_enable(subitemSave, true);        

            // Close log file
            //log_file.close();

            // Simulation final message
            if (is_correct)
                log_print("Simulation complete");
            else
                log_print("FAIL: Method iterations limit");
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void sim_printer_func()
        {
            SimData sim_data = new SimData();

            sim_data.t = simulator.get_time();
            sim_data.z1 = simulator.get_y(0);
            sim_data.z2 = simulator.get_y(1);
            sim_data.z3 = simulator.get_y(2);
            sim_data.phi1 = simulator.get_y(3);
            sim_data.phi2 = simulator.get_y(4);
            sim_data.phi3 = simulator.get_y(5);

            sim_data_list.Add(sim_data);
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private SpringData get_spring_forces(double[] Y, double t)
        {
            SpringData springs_data = new SpringData();
            springs_data.S = new double[6];
            springs_data.A = new double[6];

            double[] der_Y = simulator.get_dydt(Y, t);

            springs_data.S[0] = - m1 * der_Y[6] / 2 - m2 * der_Y[8] / 4 - I2 * der_Y[11] / 4 / Lb - I1 * der_Y[9] / 2 / a;
            springs_data.S[1] = - m1 * der_Y[6] / 2 - m2 * der_Y[8] / 4 - I2 * der_Y[11] / 4 / Lb + I1 * der_Y[9] / 2 / a;
            springs_data.S[2] = - m1 * der_Y[7] / 2 - m2 * der_Y[8] / 4 - I2 * der_Y[11] / 4 / Lb - I1 * der_Y[10] / 2 / a;
            springs_data.S[3] = -m1 * der_Y[7] / 2 - m2 * der_Y[8] / 4 - I2 * der_Y[11] / 4 / Lb + I1 * der_Y[10] / 2 / a;
            springs_data.S[4] = -m2 * der_Y[8] / 2 - I2 * der_Y[11] / 2 / Lb;
            springs_data.S[5] = -m2 * der_Y[8] / 2 + I2 * der_Y[11] / 2 / Lb;

            springs_data.A[0] = Y[0] + a * Y[3];
            springs_data.A[1] = Y[0] - a * Y[3];
            springs_data.A[2] = Y[1] + a * Y[4];
            springs_data.A[3] = Y[1] - a * Y[4];
            springs_data.A[4] = Y[2] + Lb * Y[5];
            springs_data.A[5] = Y[2] - Lb * Y[5];

            return springs_data;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void log_forces_func()
        {
            SpringData springs_data = get_spring_forces(simulator.get_y(), simulator.get_time());
            springs_data_list.Add(springs_data);
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void print_func()
        {
            string msg = String.Format(CultureInfo.InvariantCulture, 
                                       "t = {0,10:F5} z1 = {1,8:F5} z2 = {2,8:F5} z3 = {3,8:F5} phi1 = {4,8:F5} phi2 = {5,8:F5} phi3 = {6,8:F5}", 
                                       simulator.get_time(),
                                       simulator.get_y(0),
                                       simulator.get_y(1),
                                       simulator.get_y(2),
                                       simulator.get_y(3),
                                       simulator.get_y(4),
                                       simulator.get_y(5));

            log_print(msg);
        }


        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void log_print_func(StreamWriter writer)
        {
            string msg = String.Format(CultureInfo.InvariantCulture,
                                       "{0,10:F5} {1,8:F5} {2,8:F5} {3,8:F5} {4,8:F5} {5,8:F5} {6,8:F5}",
                                       simulator.get_time(),
                                       simulator.get_y(0),
                                       simulator.get_y(1),
                                       simulator.get_y(2),
                                       simulator.get_y(3),
                                       simulator.get_y(4),
                                       simulator.get_y(5));

            writer.WriteLine(msg);
        }

        //---------------------------------------------------------------------
        //      Autoscroll
        //---------------------------------------------------------------------
        private void LogBox_TextChanged(object sender, EventArgs e)
        {
            LogBox.SelectionStart = LogBox.Text.Length;
            LogBox.ScrollToCaret();
        }

        
        // Pause simulation thread
        private void subitemPause_Click(object sender, EventArgs e)
        {
            sim_pause = true;
            subitemStop.Enabled = false;
            subitemPause.Enabled = false;
            subitemStart.Enabled = true;
        }

        // Stop simulation thread
        private void subitemStop_Click(object sender, EventArgs e)
        {
            sim_stop = true;
            subitemStart.Enabled = true;
            subitemPause.Enabled = false;
            subitemStop.Enabled = false;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            quit();
        }

        private void set_subitem_enable(ToolStripMenuItem item, bool enabled)
        {
            if (MainMenu.InvokeRequired)
            {
                MainMenu.Invoke(new Action<bool>((s) => item.Enabled = s), enabled);
            }
            else
                item.Enabled = enabled;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void dynamics_calculate(double V, double fs1, double fs2, double gamma1, double gamma2, ref Dynamics dynamics)
        {
            model.set_stage1_damp_coeff(gamma1);
            model.set_stage2_damp_coeff(gamma2);
            model.set_stage1_static_deformation(fs1 / 1000.0);
            model.set_stage2_static_deformation(fs2 / 1000.0);

            model.set_velocity(V / 3.6);

            model.init();

            simulator.reset();

            bool is_correst = true;

            springs_data_list.Clear();

            while ( (simulator.get_time() <= solver_data.opt_sim_time) && (is_correst) )
            {
                double DeltaT = solver_data.t_end / solver_data.points_num;

                if (DeltaT < solver_data.max_step)
                    DeltaT = solver_data.max_step; 

                log_springs.process(simulator.get_step(), DeltaT);

                is_correst = simulator.process();
            }

            Statics statics = new Statics();
            statics.P = new double[springs_data_list[0].S.Length];

            double L = 0;
            get_texbox_double(WaveLength, ref L);

            statics.omega = 2 * Math.PI * V / 3.6 / L;
            statics.T = solver_data.opt_sim_time;

            statics.P[0] = (2 * m1 + m2) * g / 4;
            statics.P[1] = (2 * m1 + m2) * g / 4;
            statics.P[2] = (2 * m1 + m2) * g / 4;
            statics.P[3] = (2 * m1 + m2) * g / 4;
            statics.P[4] = m2 * g / 2;
            statics.P[5] = m2 * g / 2;                  

            dynamics = Statistics.get_dynamics(statics, springs_data_list);                        
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private double J(double V, double fs1, double fs2, double gamma1, double gamma2)
        {
            double ret = 0;
            Dynamics dynamics = new Dynamics();
            dynamics_calculate(V, fs1, fs2, gamma1, gamma2, ref dynamics);

            double[] pdk_max = new double[dynamics.pdk.Length];

            pdk_max[0] = pdk_max[1] = pdk_max[2] = pdk_max[3] = 0.3;
            pdk_max[4] = pdk_max[5] = 0.2;

            pdk_max[6] = 3.25;
            pdk_max[7] = 3.25;
            pdk_max[8] = 3.25;
            pdk_max[9] = 3.25;
            pdk_max[10] = 3.25;
            pdk_max[11] = 3.25;

            for (int i = 0; i < pdk_max.Length; i++)
                ret += dynamics.pdk[i] / pdk_max[i];

            return ret;         
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private double J_norm(double[] x)
        {
            double fs1 = opt_data.fs1_min + x[0] * (opt_data.fs1_max - opt_data.fs1_min);
            double fs2 = opt_data.fs2_min + x[1] * (opt_data.fs2_max - opt_data.fs2_min);
            double gamma1 = opt_data.gamma1_min + x[2] * (opt_data.gamma1_max - opt_data.gamma1_min);
            double gamma2 = opt_data.gamma2_min + x[3] * (opt_data.gamma2_max - opt_data.gamma2_min);

            return J(opt_data.v, fs1, fs2, gamma1, gamma2);
        } 

        private void optStart_Click(object sender, EventArgs e)
        {
            if (!check_init())
                return;            

            // Get data from text fields
            VehicleData vehicle_data = new VehicleData();
            RailwayData railway_data = new RailwayData();

            if (!get_vehicle_data(ref vehicle_data))
            {
                log_print("FAIL: Vehicle data are incorrect. Simulation is impossible");
                return;
            }

            if (!get_railway_data(ref railway_data))
            {
                log_print("FAIL: Vehicle data are incorrect. Simulation is impossible");
                return;
            }

            // Send data to model
            send_data_to_model(vehicle_data, railway_data);

            if (!get_opt_data(ref opt_data))
            {
                log_print("FAIL: Optimization parameters are incorrect. Optimization is impossible");
                return;
            }

            //opt_thread_func();
            if (opt_stop)
            {
                optStart.Enabled = false;
                optPause.Enabled = optStop.Enabled = true;
                opt_stop = false;

                ThreadPool.QueueUserWorkItem(new WaitCallback((s) =>
                {
                    opt_thread_func();
                }));
            }
            else
            {
                opt_ew.Set();
                optStop.Enabled = optPause.Enabled = true;
                optStart.Enabled = false;
            }
        } 

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void opt_thread_func()
        {
            Optimum opt = new VeryFastAnnealing();
            opt.set_criterium_func(this.J_norm);
            opt.read_config("../cfg/vfa.xml");

            double[] x = { 0.5, 0.5, 0.5, 0.5 };

            /*VehicleData vehicle_data = new VehicleData();

            if (!get_vehicle_data(ref vehicle_data))
                return;

            x[0] = (vehicle_data.fs1 - opt_data.fs1_min) / (opt_data.fs1_max - opt_data.fs1_min);
            x[1] = (vehicle_data.fs2 - opt_data.fs2_min) / (opt_data.fs2_max - opt_data.fs2_min);
            x[2] = (vehicle_data.gamma1 - opt_data.gamma1_min) / (opt_data.gamma1_max - opt_data.gamma1_min);
            x[3] = (vehicle_data.gamma2 - opt_data.gamma2_min) / (opt_data.gamma2_max - opt_data.gamma2_min);*/

            Random rnd = new Random();

            for (int i = 0; i < x.Length; i++)
                x[i] = rnd.NextDouble();

            string res = "";

            double delta = 0;
            double J1 = 0;

            set_button_enabled(optSendParams, false);

            log_print("Optimization is started...");   

            do
            {
                if (opt_pause)
                {
                    opt_ew.WaitOne();
                    opt_pause = false;
                }

                delta = opt.step(ref x, 0, 0, ref J1);

                res = String.Format(CultureInfo.InvariantCulture, "Iteration {0,5}: ", opt.get_iter_count());

                for (int i = 0; i < x.Length; i++)
                    res += String.Format(CultureInfo.InvariantCulture, "x[{0}] = {1,2:F4} ", i, x[i]);

                res += String.Format(CultureInfo.InvariantCulture, "delta = {0,2:F10} J = {1,2:F10}", delta, J1);

                log_print(res);                

            } while ( (delta >= opt_data.eps) && (!opt_stop) );

            if (opt_stop)
            {
                log_print("Optimization is aborted by user");

                set_button_enabled(optStart, true);
                set_button_enabled(optPause, false);
                set_button_enabled(optStop, false);

                return;
            }

            set_button_enabled(optStart, true);
            set_button_enabled(optPause, false);
            set_button_enabled(optStop, false);

            opt_stop = true;

            set_button_enabled(optSendParams, true);

            log_print("OPTIMIZATION COMPLETE!");

            opt_data.fs_1 = opt_data.fs1_min + x[0] * (opt_data.fs1_max - opt_data.fs1_min);
            opt_data.fs_2 = opt_data.fs2_min + x[1] * (opt_data.fs2_max - opt_data.fs2_min);
            opt_data.gamma_1 = opt_data.gamma1_min + x[2] * (opt_data.gamma1_max - opt_data.gamma1_min);
            opt_data.gamma_2 = opt_data.gamma2_min + x[3] * (opt_data.gamma2_max - opt_data.gamma2_min);

            res = String.Format(CultureInfo.InvariantCulture, "fs1 = {0,4:F1} fs2 = {1,4:F1} gamma1 = {2,4:F3} gamma2 = {3,4:F3}", 
                                opt_data.fs_1, opt_data.fs_2, opt_data.gamma_1, opt_data.gamma_2);

            log_print(res);

            Dynamics dyn = new Dynamics();

            dynamics_calculate(opt_data.v, opt_data.fs_1, opt_data.fs_2, opt_data.gamma_1, opt_data.gamma_2, ref dyn);

            res = "";

            for (int i = 0; i < dyn.pdk.Length; i++)
                res += String.Format(CultureInfo.InvariantCulture, "pdk[{0}] = {1,2:F4} ", i, dyn.pdk[i]);

            log_print(res);
        }
        
        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private bool get_opt_data(ref OptData opt_data)
        {
            if (!get_texbox_double(Fs1Min, ref opt_data.fs1_min))
                return false;

            if (opt_data.fs1_min <= 0)
            {
                log_print("ERROR: Stage #1 minimal static deformation is negative or zero");
                return false;
            }

            if (!get_texbox_double(Fs1Max, ref opt_data.fs1_max))
                return false;

            if (opt_data.fs1_max <= 0)
            {
                log_print("ERROR: Stage #1 maximal static deformation is negative or zero");
                return false;
            }

            if (opt_data.fs1_max <= opt_data.fs1_min)
            {
                log_print("ERROR:  Stage #1 maximal static deformation must be more that minimal");
                return false;
            }

            if (!get_texbox_double(Fs2Min, ref opt_data.fs2_min))
                return false;

            if (opt_data.fs2_min <= 0)
            {
                log_print("ERROR: Stage #2 minimal static deformation is negative or zero");
                return false;
            }

            if (!get_texbox_double(Fs2Max, ref opt_data.fs2_max))
                return false;

            if (opt_data.fs2_max <= 0)
            {
                log_print("ERROR: Stage #2 maximal static deformation is negative or zero");
                return false;
            }

            if (opt_data.fs2_max <= opt_data.fs2_min)
            {
                log_print("ERROR:  Stage #2 maximal static deformation must be more that minimal");
                return false;
            }

            //
            if (!get_texbox_double(Gamma1Min, ref opt_data.gamma1_min))
                return false;

            if (opt_data.gamma1_min <= 0)
            {
                log_print("ERROR: Stage #1 minimal damp coeff is negative or zero");
                return false;
            }

            if (!get_texbox_double(Gamma1Max, ref opt_data.gamma1_max))
                return false;

            if (opt_data.gamma1_max <= 0)
            {
                log_print("ERROR: Stage #1 maximal damp coeff is negative or zero");
                return false;
            }

            if (opt_data.gamma1_max <= opt_data.gamma1_min)
            {
                log_print("ERROR:  Stage #1 maximal damp coeff must be more that minimal");
                return false;
            }

            //
            if (!get_texbox_double(Gamma2Min, ref opt_data.gamma2_min))
                return false;

            if (opt_data.gamma2_min <= 0)
            {
                log_print("ERROR: Stage #2 minimal damp coeff is negative or zero");
                return false;
            }

            if (!get_texbox_double(Gamma2Max, ref opt_data.gamma2_max))
                return false;

            if (opt_data.gamma2_max <= 0)
            {
                log_print("ERROR: Stage #2 maximal damp coeff is negative or zero");
                return false;
            }

            if (opt_data.gamma2_max <= opt_data.gamma2_min)
            {
                log_print("ERROR:  Stage #2 maximal damp coeff must be more that minimal");
                return false;
            }

            //
            if (!get_texbox_double(ConstructVelocity, ref opt_data.v))
                return false;

            if (opt_data.v <= 0)
            {
                log_print("ERROR: Velocity is negative or zero");
                return false;
            }

            //
            if (!get_texbox_double(Accuracy, ref opt_data.eps))
                return false;

            if (opt_data.eps <= 0)
            {
                log_print("ERROR: Accuracy is negative or zero");
                return false;
            }

            return true;
        }
        
        private void set_button_enabled(Button button, bool enabled)
        {
            if (button.InvokeRequired)
                button.Invoke(new Action<bool>((s) => button.Enabled = s), enabled);
            else
                button.Enabled = enabled;
        }

        private void optPause_Click(object sender, EventArgs e)
        {
            opt_pause = true;
            optPause.Enabled = false;
            optStart.Enabled = true;
            optStop.Enabled = false;
        }

        private void optStop_Click(object sender, EventArgs e)
        {
            opt_stop = true;
            optStop.Enabled = optPause.Enabled = false;
            //optStart.Enabled = true;
        }

        private void optSendParams_Click(object sender, EventArgs e)
        {
            Stage1StaticDeformation.Text = Convert.ToString(opt_data.fs_1, CultureInfo.InvariantCulture);
            Stage2StaticDeformation.Text = Convert.ToString(opt_data.fs_2, CultureInfo.InvariantCulture);
            Stage1DampCoeff.Text = Convert.ToString(opt_data.gamma_1, CultureInfo.InvariantCulture);
            Stage2DampCoeff.Text = Convert.ToString(opt_data.gamma_2, CultureInfo.InvariantCulture);
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void LogClear(object sender, EventArgs e)
        {
            LogBox.Text = "";
        }

        private void LogSave(object sender, EventArgs e)
        {
             if (saveLog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter w = new StreamWriter(saveLog.FileName);
                w.AutoFlush = true;

                int lines = LogBox.Lines.ToArray().Length;

                for (int i = 0; i < lines; i++)
                {
                    w.WriteLine(LogBox.Lines[i]);
                }

                w.Close();
            }            
        }

        private void LogMenuPopup(object sender, EventArgs e)
        {
            LogBox.Cursor = Cursors.Arrow;
        }

        private void LogBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu contextMenu = new ContextMenu();
                contextMenu.Popup += new EventHandler(LogMenuPopup);               

                MenuItem menuItem = new MenuItem("Clear");
                menuItem.Click += new EventHandler(LogClear);
                contextMenu.MenuItems.Add(menuItem);

                menuItem = new MenuItem("Save");
                menuItem.Click += new EventHandler(LogSave);
                contextMenu.MenuItems.Add(menuItem);

                LogBox.ContextMenu = contextMenu;                
            }
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void dyncalcStart_Click(object sender, EventArgs e)
        {
            if (!check_init())
                return;

            changeSaveFile.Enabled = false;
            
            // Get data from text fields
            VehicleData vehicle_data = new VehicleData();
            RailwayData railway_data = new RailwayData();

            if (!get_vehicle_data(ref vehicle_data))
            {
                log_print("FAIL: Vehicle data are incorrect. Simulation is impossible");
                return;
            }

            if (!get_railway_data(ref railway_data))
            {
                log_print("FAIL: Vehicle data are incorrect. Simulation is impossible");
                return;
            }

            // Send data to model
            send_data_to_model(vehicle_data, railway_data);

            if (dyncalc_stop)
            {
                dyncalcStart.Enabled = false;
                dyncalcPause.Enabled = dyncalcStop.Enabled = true;
                dyncalc_stop = false;

                ThreadPool.QueueUserWorkItem(new WaitCallback((s) =>
                {
                    dyncalc_thread_func(vehicle_data, savePDKlog.FileName);
                }));
            }
            else
            {
                dyncalc_ew.Set();
                dyncalcStop.Enabled = dyncalcPause.Enabled = true;
                dyncalcStart.Enabled = false;
            }
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void dyncalc_thread_func(VehicleData vehicle_data, string log_name)
        {
            double v0 = 0;
            double v_max = 0;
            double dv = 0;

            if (!get_texbox_double(InitVelocity, ref v0))
                return;

            if (v0 <= 0)
            {
                log_print("ERROR: Initial velocity is negative or zero");
                return;
            }

            if (!get_texbox_double(MaxVelocity, ref v_max))
                return;

            if (v0 <= 0)
            {
                log_print("ERROR: Maximal velocity is negative or zero");
                return;
            }

            if (!get_texbox_double(VelocityStep, ref dv))
                return;

            if (v0 <= 0)
            {
                log_print("ERROR: Velocity step is negative or zero");
                return;
            }

            double v = v0;
            Dynamics dyn = new Dynamics();

            set_progress_bar(0);

            StreamWriter w = null;

            try
            {
                w = new StreamWriter(log_name);
                w.AutoFlush = true;
            }
            catch (Exception e)
            {
                log_print(e.Message);
                return;
            }            

            log_print("Start dymanics quality calculation....");

            while ((v <= v_max) && (!dyncalc_stop))
            {
                if (dyncalc_pause)
                {
                    dyncalc_ew.WaitOne();
                    dyncalc_pause = false;
                }

                dynamics_calculate(v, vehicle_data.fs1, vehicle_data.fs2, vehicle_data.gamma1, vehicle_data.gamma2, ref dyn);

                string res = String.Format(CultureInfo.InvariantCulture, "{0,6:F2} ", v);

                for (int i = 0; i < dyn.pdk.Length; i++)
                    res += String.Format(CultureInfo.InvariantCulture, "{0,7:F4}", dyn.pdk[i]);

                try
                {
                    w.WriteLine(res);
                }
                catch (Exception e)
                {
                    log_print(e.Message);
                    return;
                }

                v += dv;

                int pos = Convert.ToInt32(v * 100.0 / (v_max - v0));

                if (pos > 100)
                    pos = 100;

                set_progress_bar(pos);
            }

            dyncalc_stop = true;            
            set_button_enabled(dyncalcStart, true);
            set_button_enabled(dyncalcPause, false);
            set_button_enabled(dyncalcStop, false);
            set_button_enabled(changeSaveFile, true);

            if (w != null)
                w.Close();

            string cur_dir = Directory.GetCurrentDirectory();
            string gnuplot_path = cur_dir.Replace("bin", GNUPLOT_FOLDER + "gnuplot.exe");
            string work_dir = Path.GetDirectoryName(savePDKlog.FileName);

            try
            { 
                gnuplot_process_start(gnuplot_path, work_dir, savePDKlog.FileName);
            }
            catch (Exception ex)
            {
                log_print(ex.Message);
                log_print("Graphics output is failure!");
                return;
            }

            log_print("Done");
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void set_progress_bar(int value)
        {
            if (MainStatus.InvokeRequired)
                MainStatus.Invoke(new Action<int>((s) => optProgress.Value = s), value);
            else
                optProgress.Value = value;
        }       

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        private void gnuplot_process_start(string exec_path, string work_dir, string log_file)
        {
            ProcessStartInfo psi = new ProcessStartInfo(exec_path);
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = true;
            psi.RedirectStandardOutput = true;

            //string cur_dir = Directory.GetCurrentDirectory();
            psi.WorkingDirectory = work_dir;
            psi.CreateNoWindow = true;

            using (Process proc = Process.Start(psi))
            {
                string prefix = Path.GetFileNameWithoutExtension(log_file);

                StreamWriter w = proc.StandardInput;
                StreamReader r = proc.StandardOutput;
                w.AutoFlush = true;

                w.WriteLine("set xzeroaxis lt -1");
                w.WriteLine("set yzeroaxis lt -1");
                w.WriteLine("set xrange [0:]");
                w.WriteLine("set yrange [0:]");
                w.WriteLine("set grid xtics lc rgb '#555555' lw 1 lt 0");
                w.WriteLine("set grid ytics lc rgb '#555555' lw 1 lt 0");
                w.WriteLine("set xlabel 'v, км/ч'");
                w.WriteLine("set ylabel 'Kvd'");
                w.WriteLine("set key top left");
                w.WriteLine("set terminal emf 'Arial' 12");        
                w.WriteLine("set terminal emf color solid");
                w.WriteLine("set size ratio 0.5");

                // First stage springs Kvd
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "set output '{0}_first_stage_Kvd.emf'", prefix));

                string plot_cmd = "plot ";
                
                for (int i = 1; i <= 4; i++)
                    plot_cmd += String.Format(CultureInfo.InvariantCulture, "'{2}' using 1:{0} with lines lw 2 ti 'Комплект {1}',", i+1, i, log_file);

                w.WriteLine(plot_cmd);

                // First stage springs Kvd
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "set output '{0}_second_stage_Kvd.emf'", prefix));

                plot_cmd = "plot ";

                for (int i = 1; i <= 2; i++)
                    plot_cmd += String.Format(CultureInfo.InvariantCulture, "'{2}' using 1:{0} with lines lw 2 ti 'Опора {1}',", i+5, i, log_file);

                w.WriteLine(plot_cmd);

                // 
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "set output '{0}_second_stage_C.emf'", prefix));
                w.WriteLine("set ylabel 'C'");

                plot_cmd = "plot ";

                for (int i = 12; i <= 13; i++)
                    plot_cmd += String.Format(CultureInfo.InvariantCulture, "'{2}' using 1:{0} with lines lw 2 ti 'Опора {1}',", i, i - 11, log_file);

                w.WriteLine(plot_cmd);

                w.Close();

                proc.WaitForExit();
            }

            log_print("Graphics generation done.");           
        }

        private void dyncalcPause_Click(object sender, EventArgs e)
        {
            dyncalc_pause = true;
            dyncalcStart.Enabled = true;
            dyncalcPause.Enabled = dyncalcStop.Enabled = false; 
        }

        private void dyncalcStop_Click(object sender, EventArgs e)
        {
            dyncalc_stop = true;
            dyncalcStart.Enabled = true;
            dyncalcPause.Enabled = dyncalcStop.Enabled = false;
            changeSaveFile.Enabled = true;
        }

        private void changeSaveFile_Click(object sender, EventArgs e)
        {
            savePDKlog.ShowDialog();
        }

        private void subitemOptions_Click(object sender, EventArgs e)
        {
            options_form_show();
        }

        private void options_form_show()
        {
            options = new Form();
            options.MaximizeBox = false;
            options.MinimizeBox = false;
            options.FormBorderStyle = FormBorderStyle.Fixed3D;
            options.Size = new Size(280, 310);
            options.Text = "Simulation options";
            options.Icon = this.Icon;

            TextBox InitTime = new TextBox();
            InitTime.Name = "InitTime";
            InitTime.Size = new Size(55, 30);
            InitTime.Location = new Point(160, 30);
            InitTime.Text = Convert.ToString(solver_data.t_begin, CultureInfo.InvariantCulture);
            options.Controls.Add(InitTime);

            TextBox EndTime = new TextBox();
            EndTime.Name = "EndTime";
            EndTime.Size = new Size(55, 30);
            EndTime.Location = new Point(160, 60);
            EndTime.Text = Convert.ToString(solver_data.t_end, CultureInfo.InvariantCulture);
            options.Controls.Add(EndTime);

            TextBox MaxStep = new TextBox();
            MaxStep.Name = "MaxStep";
            MaxStep.Size = new Size(55, 30);
            MaxStep.Location = new Point(160, 90);
            MaxStep.Text = Convert.ToString(solver_data.max_step, CultureInfo.InvariantCulture);
            options.Controls.Add(MaxStep);

            TextBox LocalErr = new TextBox();
            LocalErr.Name = "LocalErr";
            LocalErr.Size = new Size(55, 30);
            LocalErr.Location = new Point(160, 120);
            LocalErr.Text = Convert.ToString(solver_data.local_err, CultureInfo.InvariantCulture);
            options.Controls.Add(LocalErr);            

            ComboBox IntMethod = new ComboBox();
            IntMethod.Name = "IntMethod";
            IntMethod.Size = new Size(55, 30);
            IntMethod.Location = new Point(160, 150);
            IntMethod.Items.Add("rkf5");
            IntMethod.Items.Add("rk4");
            IntMethod.Items.Add("euler");
            IntMethod.SelectedIndex = IntMethod.Items.IndexOf(solver_data.method);
            options.Controls.Add(IntMethod);

            TextBox SavedPoints = new TextBox();
            SavedPoints.Name = "SavedPoints";
            SavedPoints.Size = new Size(55, 30);
            SavedPoints.Location = new Point(160, 180);
            SavedPoints.Text = Convert.ToString(solver_data.points_to_simlog, CultureInfo.InvariantCulture);
            options.Controls.Add(SavedPoints);

            Button Apply = new Button();
            Apply.Name = "Apply";
            Apply.Size = new Size(60, 27);
            Apply.Location = new Point(102, 220);
            Apply.Font = optStart.Font;
            Apply.Click += new EventHandler(optionsApply_Click);
            Apply.Text = "Apply";
            options.Controls.Add(Apply);

            Button OK = new Button();
            OK.Name = "OK";
            OK.Size = new Size(60, 27);
            OK.Location = new Point(37, 220);
            OK.Font = optStart.Font;
            OK.Click += new EventHandler(optionsOK_Click);
            OK.Text = "OK";
            options.Controls.Add(OK);

            Button Cancel = new Button();
            Cancel.Name = "Cancel";
            Cancel.Size = new Size(60, 27);
            Cancel.Location = new Point(167, 220);
            Cancel.Font = optStart.Font;
            Cancel.Click += new EventHandler(optionsCancel_Click);
            Cancel.Text = "Cancel";
            options.Controls.Add(Cancel);

            Label init_time = new Label();
            init_time.Text = "Initial time, s";
            init_time.Location = new Point(50, 30);
            options.Controls.Add(init_time);

            Label end_time = new Label();
            end_time.Text = "End time, s";
            end_time.Location = new Point(50, 60);
            options.Controls.Add(end_time);

            Label time_step = new Label();
            time_step.Text = "Maxiaml time step, s";
            time_step.Location = new Point(50, 90);
            options.Controls.Add(time_step);

            Label local_err = new Label();
            local_err.Text = "Local error";
            local_err.Location = new Point(50, 120);
            options.Controls.Add(local_err);

            Label int_method = new Label();
            int_method.Text = "Integration method";
            int_method.Location = new Point(50, 150);
            options.Controls.Add(int_method);

            Label saved_points = new Label();
            saved_points.Text = "Number points";
            saved_points.Location = new Point(50, 180);
            options.Controls.Add(saved_points);

            options.ShowDialog();
        }

        private void optionsApply_Click(object sender, EventArgs e)
        {
            SolverData solver_data = this.solver_data;

            if (!get_texbox_double((TextBox)options.Controls["InitTime"], ref solver_data.t_begin))
                return;

            if (solver_data.t_begin < 0)
            {
                log_print("ERROR: Initial time is negative");
                return;
            }

            if (!get_texbox_double((TextBox)options.Controls["EndTime"], ref solver_data.t_end))
                return;

            if (solver_data.t_end < 0)
            {
                log_print("ERROR: End time is negative");
                return;
            }

            if (solver_data.t_begin >= solver_data.t_end)
            {
                log_print("ERROR: Initial time is mode or equal that end time");
                return;
            }

            if (!get_texbox_double((TextBox)options.Controls["MaxStep"], ref solver_data.max_step))
                return;

            if (solver_data.max_step <= 0)
            {
                log_print("ERROR: Max step is negative or zero");
                return;
            }

            if (!get_texbox_double((TextBox)options.Controls["LocalErr"], ref solver_data.local_err))
                return;

            if (solver_data.local_err <= 0)
            {
                log_print("ERROR: Local error is negative or zero");
                return;
            }

            solver_data.method = ((ComboBox)options.Controls["IntMethod"]).SelectedItem.ToString();

            this.solver_data = solver_data;

            set_simulation_params(this.solver_data);
        }

        private void optionsOK_Click(object sender, EventArgs e)
        {
            ((Button)options.Controls["Apply"]).PerformClick();

            options.Close();
        }

        private void optionsCancel_Click(object sender, EventArgs e)
        {
            options.Close();
        }

        private void subitemSave_Click(object sender, EventArgs e)
        {
            if (SimSave.ShowDialog() == DialogResult.OK)
            {
                save_sim_data(SimSave.FileName);
            }
        }

        private void save_sim_data(string file_name)
        {
            StreamWriter w = null;

            try
            {
                w = new StreamWriter(file_name);
                w.AutoFlush = true;
            }
            catch (Exception e)
            {
                log_print(e.Message.ToString());
                return;
            }            

            for (int i = 0; i < sim_data_list.Count; i++)
            {
                SimData sim_data = sim_data_list[i];
                string data = String.Format(CultureInfo.InvariantCulture, "{0,10:F4} {1,8:F5} {2,8:F5} {3,8:F5} {4,8:F5} {5,8:F5} {6,8:F5}", 
                                            sim_data.t,
                                            sim_data.z1,
                                            sim_data.z2,
                                            sim_data.z3,
                                            sim_data.phi1,
                                            sim_data.phi2,
                                            sim_data.phi3);

                try
                {
                    w.WriteLine(data);
                }                
                catch (Exception e)
                {
                    log_print(e.Message.ToString());
                    return;
                }
            }

            if (w != null)
                w.Close();

            string cur_dir = Directory.GetCurrentDirectory();
            string gnuplot_path = cur_dir.Replace("bin", GNUPLOT_FOLDER + "gnuplot.exe");
            string work_dir = Path.GetDirectoryName(SimSave.FileName);

            gnuplot_sim_graphs(gnuplot_path, work_dir, SimSave.FileName);
        }

        private void gnuplot_sim_graphs(string exec_path, string work_dir, string log_file)
        {
            ProcessStartInfo psi = new ProcessStartInfo(exec_path);
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = true;
            psi.RedirectStandardOutput = true;

            //string cur_dir = Directory.GetCurrentDirectory();
            psi.WorkingDirectory = work_dir;
            psi.CreateNoWindow = true;

            using (Process proc = Process.Start(psi))
            {
                string prefix = Path.GetFileNameWithoutExtension(log_file);

                StreamWriter w = proc.StandardInput;
                StreamReader r = proc.StandardOutput;
                w.AutoFlush = true;

                w.WriteLine("set xzeroaxis lt -1");
                w.WriteLine("set yzeroaxis lt -1");               
                w.WriteLine("set grid xtics lc rgb '#555555' lw 1 lt 0");
                w.WriteLine("set grid ytics lc rgb '#555555' lw 1 lt 0");
                w.WriteLine("set key top left");
                w.WriteLine("set terminal emf 'Arial' 12");
                w.WriteLine("set terminal emf color solid");
                w.WriteLine("set size ratio 0.3");

                w.WriteLine("set xlabel 't, с'");
                w.WriteLine("set ylabel 'z1, м'");
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "set output '{0}-z1.emf'", prefix));
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "plot '{0}' using 1:2 with lines lw 2 noti", log_file));

                w.WriteLine("set ylabel 'z2, м'");
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "set output '{0}-z2.emf'", prefix));
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "plot '{0}' using 1:3 with lines lw 2 noti", log_file));

                w.WriteLine("set ylabel 'z3, м'");
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "set output '{0}-z3.emf'", prefix));
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "plot '{0}' using 1:4 with lines lw 2 noti", log_file));

                w.WriteLine("set ylabel 'phi1, рад'");
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "set output '{0}-phi1.emf'", prefix));
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "plot '{0}' using 1:5 with lines lw 2 noti", log_file));

                w.WriteLine("set ylabel 'phi2, рад'");
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "set output '{0}-phi2.emf'", prefix));
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "plot '{0}' using 1:6 with lines lw 2 noti", log_file));

                w.WriteLine("set ylabel 'phi3, рад'");
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "set output '{0}-phi3.emf'", prefix));
                w.WriteLine(String.Format(CultureInfo.InvariantCulture, "plot '{0}' using 1:6 with lines lw 2 noti", log_file));

                w.Close();
            }

            log_print("Simulation results graphics are printed successful");
        }

        private void itemAbout_Click(object sender, EventArgs e)
        {
            about = new Form();
            about.Size = new Size(640, 480);
            about.MaximizeBox = false;
            about.MinimizeBox = false;
            about.FormBorderStyle = FormBorderStyle.Fixed3D;
            about.Icon = this.Icon;
            about.Text = "Vertical Dymanics v 0.1.0";

            RichTextBox license = new RichTextBox();
            license.Size = new Size(about.ClientSize.Width - 60,  about.ClientSize.Height - 130);
            license.Location = new Point(30, 30);
            license.ReadOnly = true;

            try
            {
                license.LoadFile("../LICENSE.rtf");
            }
            catch (Exception ex)
            {
                log_print(ex.Message.ToString());
                return;
            }
            
            about.Controls.Add(license);

            Label copyleft = new Label();            
            copyleft.AutoSize = true;
            copyleft.AutoEllipsis = true;            
            copyleft.Font = new Font(FontFamily.GenericSansSerif, 9.0f);
            copyleft.Location = new Point(30, about.ClientSize.Height - 90);
            copyleft.Text = "(c) PhD, Dmitry Pritykin, Rostov State Transport University, Electric railway transport chair. March, 2016";
            about.Controls.Add(copyleft);

            Button OK = new Button();
            OK.Name = "OK";
            OK.Size = new Size(60, 27);
            OK.Location = new Point(about.ClientSize.Width/2 - OK.Size.Width/2, about.ClientSize.Height - 40);
            OK.Font = optStart.Font;
            OK.Click += new EventHandler(aboutOK_Click);
            OK.Text = "OK";
            about.Controls.Add(OK);

            about.ShowDialog();
        }

        private void aboutOK_Click(object sender, EventArgs e)
        {
            about.Close();
        }
    }
}
