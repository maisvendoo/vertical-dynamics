﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace solver
{
    public struct ModelStatus
    {
        public string msg;
        public bool ready;
    }
    
    //-------------------------------------------------------------------------
    //      Model access class
    //-------------------------------------------------------------------------
    class Model
    {
        public const string M_OK = "Model loaded from ";

        private const string METHOD_GET_DIMENTION = "get_dimention";
        private const string METHOD_INIT = "init";
        private const string METHOD_dFdT = "dFdt";
        private const string METHOD_PRE_STEP = "pre_step";
        private const string METHOD_POST_STEP = "post_step";
        private const string METHOD_SET_BODY_MASS = "set_body_mass";
        private const string METHOD_SET_TROLLEY_MASS = "set_trolley_mass";
        private const string METHOD_SET_BODY_MOMENT_OF_INERTIA = "set_body_moment_of_inertia";
        private const string METHOD_SET_TROLLEY_MOMENT_OF_INERTIA = "set_trolley_moment_of_inertia";
        private const string METHOD_SET_STAGE1_STATIC_DEFORMATION = "set_stage1_static_deformation";
        private const string METHOD_SET_STAGE2_STATIC_DEFORMATION = "set_stage2_static_deformation";
        private const string METHOD_SET_STAGE1_DAMP_COEFF = "set_stage1_damp_coeff";
        private const string METHOD_SET_STAGE2_DAMP_COEFF = "set_stage2_damp_coeff";
        private const string METHOD_SET_TROLLEY_HALF_BASE = "set_trolley_half_base";
        private const string METHOD_SET_BODY_HALF_BASE = "set_body_half_base";
        private const string METHOD_SET_VELOCITY = "set_velocity";
        private const string METHOD_SET_WAVE_LENGTH = "set_wave_length";
        private const string METHOD_SET_AMPLIFY = "set_amplify";
        private const string METHOD_SET_RANDOM_LAW = "set_random_law";

        private Assembly model_dll = null;
        private Object instance = null;

        private MethodInfo mi_init;
        private MethodInfo mi_get_demention;
        private MethodInfo mi_dfdt;
        private MethodInfo mi_pre_step;
        private MethodInfo mi_post_step;
        private MethodInfo mi_set_body_mass;
        private MethodInfo mi_set_trolley_mass;
        private MethodInfo mi_set_body_moment_of_inertia;
        private MethodInfo mi_set_trolley_moment_of_inertia;
        private MethodInfo mi_set_stage1_static_deformation;
        private MethodInfo mi_set_stage2_static_deformation;
        private MethodInfo mi_set_stage1_damp_coeff;
        private MethodInfo mi_set_stage2_damp_coeff;
        private MethodInfo mi_set_trolley_half_base;
        private MethodInfo mi_set_body_half_base;
        private MethodInfo mi_set_velocity;
        private MethodInfo mi_set_wave_length;
        private MethodInfo mi_set_amplify;
        private MethodInfo mi_set_random_law;

        //---------------------------------------------------------------------
        //      Constructor
        //---------------------------------------------------------------------
        public Model()
        {
            
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public ModelStatus load(string dll_name)
        {
            ModelStatus status = new ModelStatus{ msg = "", ready = false };
            
            // Try to load model's DLL library
            try
            {
                model_dll = Assembly.LoadFrom(dll_name);
            }
            catch (Exception ex)
            {
                status.msg = ex.Message.ToString();
                status.ready = false;
                return status;
            }

            // Get model constructor instance
            Type type = model_dll.GetType("model.CModel");

            try
            {
                instance = Activator.CreateInstance(type);                
            }
            catch (Exception ex)
            {
                status.msg = ex.Message.ToString();
                status.ready = false;
                return status;
            }

            // Create reflecions of all public methods
            mi_init = type.GetMethod(METHOD_INIT);

            if (mi_init == null)
            {
                status.msg = "Method " + METHOD_INIT + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_get_demention = type.GetMethod(METHOD_GET_DIMENTION);

            if (mi_get_demention == null)
            {
                status.msg = "Method " + METHOD_GET_DIMENTION + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_dfdt = type.GetMethod(METHOD_dFdT);

            if (mi_dfdt == null)
            {
                status.msg = "Method " + METHOD_dFdT + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_pre_step = type.GetMethod(METHOD_PRE_STEP);

            if (mi_pre_step == null)
            {
                status.msg = "Method " + METHOD_PRE_STEP + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_post_step = type.GetMethod(METHOD_PRE_STEP);

            if (mi_post_step == null)
            {
                status.msg = "Method " + METHOD_POST_STEP + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_body_mass = type.GetMethod(METHOD_SET_BODY_MASS);

            if (mi_set_body_mass == null)
            {
                status.msg = "Method " + METHOD_SET_BODY_MASS + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_trolley_mass = type.GetMethod(METHOD_SET_TROLLEY_MASS);

            if (mi_set_trolley_mass == null)
            {
                status.msg = "Method " + METHOD_SET_TROLLEY_MASS + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_trolley_moment_of_inertia = type.GetMethod(METHOD_SET_TROLLEY_MOMENT_OF_INERTIA);

            if (mi_set_trolley_moment_of_inertia == null)
            {
                status.msg = "Method " + METHOD_SET_TROLLEY_MOMENT_OF_INERTIA + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_body_moment_of_inertia = type.GetMethod(METHOD_SET_BODY_MOMENT_OF_INERTIA);

            if (mi_set_body_moment_of_inertia == null)
            {
                status.msg = "Method " + METHOD_SET_BODY_MOMENT_OF_INERTIA + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_stage1_static_deformation = type.GetMethod(METHOD_SET_STAGE1_STATIC_DEFORMATION);

            if (mi_set_stage1_static_deformation == null)
            {
                status.msg = "Method " + METHOD_SET_STAGE1_STATIC_DEFORMATION + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_stage2_static_deformation = type.GetMethod(METHOD_SET_STAGE2_STATIC_DEFORMATION);

            if (mi_set_stage2_static_deformation == null)
            {
                status.msg = "Method " + METHOD_SET_STAGE2_STATIC_DEFORMATION + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_stage1_damp_coeff = type.GetMethod(METHOD_SET_STAGE1_DAMP_COEFF);

            if (mi_set_stage1_damp_coeff == null)
            {
                status.msg = "Method " + METHOD_SET_STAGE1_DAMP_COEFF + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_stage2_damp_coeff = type.GetMethod(METHOD_SET_STAGE2_DAMP_COEFF);

            if (mi_set_stage2_damp_coeff == null)
            {
                status.msg = "Method " + METHOD_SET_STAGE2_DAMP_COEFF + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_trolley_half_base = type.GetMethod(METHOD_SET_TROLLEY_HALF_BASE);

            if (mi_set_trolley_half_base == null)
            {
                status.msg = "Method " + METHOD_SET_TROLLEY_HALF_BASE + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_body_half_base = type.GetMethod(METHOD_SET_BODY_HALF_BASE);

            if (mi_set_body_half_base == null)
            {
                status.msg = "Method " + METHOD_SET_BODY_HALF_BASE + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_velocity = type.GetMethod(METHOD_SET_VELOCITY);

            if (mi_set_velocity == null)
            {
                status.msg = "Method " + METHOD_SET_VELOCITY + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_wave_length = type.GetMethod(METHOD_SET_WAVE_LENGTH);

            if (mi_set_wave_length == null)
            {
                status.msg = "Method " + METHOD_SET_WAVE_LENGTH + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_amplify = type.GetMethod(METHOD_SET_AMPLIFY);

            if (mi_set_amplify == null)
            {
                status.msg = "Method " + METHOD_SET_AMPLIFY + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            mi_set_random_law = type.GetMethod(METHOD_SET_RANDOM_LAW);

            if (mi_set_random_law == null)
            {
                status.msg = "Method " + METHOD_SET_RANDOM_LAW + "() is not found in " + dll_name;
                status.ready = false;
                return status;
            }

            status.msg = "Model is loaded from " + dll_name + " successfully";
            status.ready = true;

            return status;
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void init()
        {
            mi_init.Invoke(instance, new object[] { });
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public int get_dimention()
        {
            return (int) mi_get_demention.Invoke(instance, new object[] { });
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void dFdt(double[] Y, ref double[] dYdt, double t)
        {
            mi_dfdt.Invoke(instance, new object[] { Y, dYdt, t });
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void pre_step(ref double[] Y, double t)
        {
            mi_pre_step.Invoke(instance, new object[] { Y, t });
        }

        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        public void post_step(ref double[] Y, double t)
        {
            mi_post_step.Invoke(instance, new object[] { Y, t });
        }

        public void set_body_mass(double m2)
        {
            mi_set_body_mass.Invoke(instance, new object[] { m2 });
        }

        public void set_trolley_mass(double m1)
        {
            mi_set_trolley_mass.Invoke(instance, new object[] { m1 });
        }

        public void set_trolley_moment_of_inertia(double I1)
        {
            mi_set_trolley_moment_of_inertia.Invoke(instance, new object[] { I1 });
        }

        public void set_body_moment_of_inertia(double I2)
        {
            mi_set_body_moment_of_inertia.Invoke(instance, new object[] { I2 });
        }

        public void set_stage1_static_deformation(double fs1)
        {
            mi_set_stage1_static_deformation.Invoke(instance, new object[] { fs1 });
        }

        public void set_stage2_static_deformation(double fs2)
        {
            mi_set_stage2_static_deformation.Invoke(instance, new object[] { fs2 });
        }

        public void set_stage1_damp_coeff(double gamma1)
        {
            mi_set_stage1_damp_coeff.Invoke(instance, new object[] { gamma1 });
        }

        public void set_stage2_damp_coeff(double gamma2)
        {
            mi_set_stage2_damp_coeff.Invoke(instance, new object[] { gamma2 });
        }

        public void set_trolley_half_base(double a)
        {
            mi_set_trolley_half_base.Invoke(instance, new object[] { a });
        }

        public void set_body_half_base(double Lb)
        {
            mi_set_body_half_base.Invoke(instance, new object[] { Lb });
        }

        public void set_velocity(double v)
        {
            mi_set_velocity.Invoke(instance, new object[] { v });
        }

        public void set_wave_lenght(double L)
        {
            mi_set_wave_length.Invoke(instance, new object[] { L });
        }

        public void set_amplify(double eta_m)
        {
            mi_set_amplify.Invoke(instance, new object[] { eta_m });
        }

        public void set_random_law(bool random_law)
        {
            mi_set_random_law.Invoke(instance, new object[] { random_law });
        }
    }
}
