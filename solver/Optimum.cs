﻿using System;

namespace solver
{
    public abstract class Optimum
    {
        protected int k;
        public Optimum()
        {
            k = 1;
        }

        protected Criterium J;

        public void set_criterium_func(Criterium J)
        {
            this.J = J;
        }

        abstract public double step(ref double[] x, double h, double lambda, ref double J1);

        protected double limitation(double x, double x_min, double x_max)
        {
            if (x < x_min)
                x = x_min;

            if (x > x_max)
                x = x_max;

            return x;
        }

        public int get_iter_count()
        {
            return k;
        }

        public abstract bool read_config(string cfg_pafh);        
    }
}