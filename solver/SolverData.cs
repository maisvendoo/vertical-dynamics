﻿using System;

namespace solver
{
    public struct SolverData
    {
        public string model_dll;
        public string method;
        public double t_begin;
        public double t_end;
        public double max_step;
        public double local_err;
        public double opt_sim_time;
        public int points_num;
        public int points_to_simlog;                   
    }

    public struct VehicleData
    {
        public double mass;
        public double trolley_coeff;
        public double body_base;
        public double body_height;
        public double trolley_base;
        public double trolley_height;
        public double construct_velocity;

        public double fs1;
        public double fs2;
        public double gamma1;
        public double gamma2;
    }

    public struct RailwayData
    {
        public double wave_length;
        public double amplify;
        public bool random_law;
    }

    public struct Dynamics
    {
        public double[] pdk;        
    }   

    public struct Statics
    {
        public double[] P;
        public double T;
        public double omega;              
    }

    public struct SpringData
    {
        public double[] S;
        public double[] A;
    }

    public struct OptData
    {
        public double fs1_min;
        public double fs1_max;
        public double fs2_min;
        public double fs2_max;
        public double gamma1_min;
        public double gamma1_max;
        public double gamma2_min;
        public double gamma2_max;
        public double v;

        public double eps;

        public double fs_1;
        public double fs_2;
        public double gamma_1;
        public double gamma_2;       
    } 
    
    public class Complex
    {
        public double Re;
        public double Im;
        
        public double Abs
        {
            get
            {
                return Math.Sqrt(Re * Re + Im * Im);
            }
        }

        public double Arg
        {
            get
            {
                if (Abs == 0)
                    return 0;

                return Math.Atan2(Im / Abs, Re / Abs);
            }
        }

        public double Amplify;
        public double Phi
        {
            get
            {
                return Arg;
            }
        }

        public double omega;
    } 
    
    public struct SimData
    {
        public double t;
        public double z1;
        public double z2;
        public double z3;
        public double phi1;
        public double phi2;
        public double phi3;
    }  
}
