﻿using System;
using System.Collections.Generic;
using System.IO;

namespace solver
{
    public class Statistics
    {
        public Statistics()
        {

        }

        public static Dynamics get_dynamics(Statics statics, List<SpringData> springs_data_list)
        {
            Dynamics dynamics = new Dynamics();

            int n = springs_data_list.Count;
            int m = springs_data_list[0].S.Length;
            int a = springs_data_list[0].A.Length;

            double[] sum_S = new double[m];
            double[] sum_A = new double[m];
            double[] mid_S = new double[m];
            double[] mid_A = new double[m];
            double[] sigma_S = new double[m];
            double[] sigma_A = new double[m];
            dynamics.pdk = new double[m+a];            

            for (int j = 0; j < m; j++)
                sum_S[j] = 0;

            // Middle values of forces and amplifies
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    sum_S[j] += springs_data_list[i].S[j];
                    sum_A[j] += springs_data_list[i].A[j];
                }                
            }

            for (int j = 0; j < m; j++)
            {
                mid_S[j] = sum_S[j] / n;
                sum_S[j] = 0;

                mid_A[j] = sum_A[j] / n;
                sum_A[j] = 0;
            }

            // Sigmas od forces and amplifies
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    sum_S[j] += (springs_data_list[i].S[j] - mid_S[j]) * (springs_data_list[i].S[j] - mid_S[j]);
                    sum_A[j] += (springs_data_list[i].A[j] - mid_A[j]) * (springs_data_list[i].A[j] - mid_A[j]);
                }
            }            

            // Vertical dynamics coeff and ride factor calculation
            for (int j = 0; j < m; j++)
            {
                sigma_S[j] = Math.Sqrt(sum_S[j] / (n - 1));
                dynamics.pdk[j] = 3 * sigma_S[j] / statics.P[j];

                sigma_A[j] = Math.Sqrt(sum_A[j] / (n - 1));
                double z0 = 3 * sigma_A[j];

                double Kf = FreqCoeff.get_Kf(statics.omega);

                dynamics.pdk[j + m] = Kf * Math.Pow(z0, 0.3) * Math.Pow(statics.omega, 0.5);             
            }            

            return dynamics;
        }

        private static double Max(double[] x, int idx1, int idx2)
        {
            double max = x[idx1];

            for (int i = idx1; i <= idx2; i++)
                if (max < x[i])
                {
                    max = x[i];
                }

            return max;
        }               
    }
}