﻿using System;
using System.Xml;
using System.Globalization;

namespace solver
{
    public class VeryFastAnnealing : Optimum
    {
        double T0 = 1.0e-4;        
        double mi = 1.0;
        double ni = 160.0;
        double Q = 1.0;
        double E0 = 1e10;

        Random rnd;
        public VeryFastAnnealing()
        {
            rnd = new Random();
        }

        public override double step(ref double[] x, double diff_step, double lambda, ref double J1)
        {
            int n = x.Length;
            double c = mi * Math.Exp(-ni / n);
            double s = c * Math.Pow(k, Q / n);
            double T = T0 * Math.Exp(-s);           
            double a = 0;
            double h = 0;
            double E1 = 0;
            double dE;

            double E = J(x);

            if (E < E0)
                E0 = E;

            double[] z = new double[n];
            double[] x1 = new double[n];

            do
            {
                for (int i = 0; i < n; i++)
                {
                    a = rnd.NextDouble();
                    z[i] = Math.Sign(a - 0.5) * T * (Math.Pow((1 + 1 / T), Math.Abs(2 * a - 1)) - 1);

                    x1[i] = x[i] + z[i];

                    x1[i] = limitation(x1[i], 0.0, 1.0);
                }

                E1 = J(x1);

                a = rnd.NextDouble();

                dE = E1 - E0;
                h = 1 / (1 + Math.Exp(dE / T));

            } while (a > h);

            J1 = E0 = E1;

            for (int i = 0; i < n; i++)
            {
                x[i] = x1[i];
            }

            k++;

            return Math.Abs(dE);
        }

        public override bool read_config(string cfg_path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(cfg_path);

            var nodes = doc.GetElementsByTagName("VeryFastAnnealing");

            foreach (XmlNode node in nodes)
            {
                foreach (XmlNode subnode in node.ChildNodes)
                {
                    if (subnode.Name == "InitTemperature")
                    {
                        T0 = Convert.ToDouble(subnode.InnerText, CultureInfo.InvariantCulture);
                    }

                    if (subnode.Name == "m")
                    {
                        mi = Convert.ToDouble(subnode.InnerText, CultureInfo.InvariantCulture);
                    }

                    if (subnode.Name == "n")
                    {
                        ni = Convert.ToDouble(subnode.InnerText, CultureInfo.InvariantCulture);
                    }

                    if (subnode.Name == "Q")
                    {
                        Q = Convert.ToDouble(subnode.InnerText, CultureInfo.InvariantCulture);
                    }
                }
            }

            return true;
        }
    }
}